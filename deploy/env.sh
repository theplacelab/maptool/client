#!/bin/bash
# Note this is a BASH script, be careful when running on CI that bash is available
# Might need: RUN apk add --no-cache bash
# Adapted from technique described here:
# https://medium.freecodecamp.org/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70

# Rebuild output file
INPUT=$1
OUTPUT=$2
echo ------------------------------------------------------
echo "IN: $INPUT"
echo "OUT: $OUTPUT"

rm -rf $OUTPUT
touch $OUTPUT
# Add assignment
echo "// This is a generated file, do not edit" >> $OUTPUT
echo "window._env_ = {" >> $OUTPUT

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    if [[ $line == *"VITE_"* ]]; then
      varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
      varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
        # Read value of current variable if exists as environment variable
        # This allows us to overwrite from docker settings
        value=$(printf '%s\n' "${!varname}")
        varvalue=$(printf '%s' "$varvalue" | sed -e "s/\"//g")

        # Otherwise use value from .env file
        [[ -z $value ]] && value=${varvalue}

        # Append as kvp to JS file
        echo "  $varname: '$value'," >> $OUTPUT
    fi
  fi
done < $INPUT
echo "};" >> $OUTPUT
cat $OUTPUT

#if [ -d "public" ]; then
#  cat public/index.html | sed "s/\(cacheburst=.*\)\"/cacheburst=\"$(uuidgen)\"/g"  > public/index.html
#else
#  cat index.html | sed "s/\(cacheburst=.*\)\"/cacheburst=\"$(uuidgen)\"/g"  > index.html
#fi
echo ------------------------------------------------------