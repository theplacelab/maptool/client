import Papa from "papaparse";
import { JSONParse } from "src/utils";

const IMPORT_TYPE = {
  Vb_POI: {
    NAME: "Maptool_Vb_S_POI_WITH_DATA",
    SIGNATURE:
      '["map_id","poi_id","representation_id","representation_config","poi_data_id","poi_name","poi_date","poi_coordinate","poi_sortorder","is_public","is_locked","is_markdown","field_data_type","field","search_scope","field_value","format_id","options"]'
  },
  V1_POI: {
    NAME: "Maptool_V1_S_POI_WITH_DATA",
    SIGNATURE:
      '["map_id","poi_id","representation_id","representation_config","presentation_id","presentation_config","poi_data_id","poi_name","poi_date","poi_coordinate","poi_sortorder","is_public","is_locked","is_markdown","field_data_type","field","search_scope","field_value","format_id","options"]',
    PARSER: (r) => {
      const acc = {
        raw: r,
        data: []
      };
      const upsert = (arr, row) => {
        const idx = Number(row.poi_id);
        const find = arr.findIndex((i) => i.id === idx);
        const field = {
          field: row.field,
          search_scope: row.search_scope,
          field_value: row.field_value,
          format_id: row.format_id,
          options: row.options
        };
        if (find === -1) {
          arr.push({
            name: row.poi_name,
            date: row.poi_date,
            id: idx,
            coordinates: JSONParse(row.poi_coordinate),
            fields: [field]
          });
        } else {
          arr[find].fields.push(field);
        }
      };

      r.data.forEach((row) => {
        const id = Number(row.poi_id);
        if (isNaN(id)) return;
        upsert(acc.data, row);
      });

      return acc;
    }
  }
};

export const importPoiFromFile = async (cb) => {
  let file;
  try {
    const [fileHandle] = await window.showOpenFilePicker({
      types: [
        {
          description: "Images",
          accept: {
            "text/csv": [".csv"]
          }
        }
      ],
      excludeAcceptAllOption: true,
      multiple: false
    });
    file = await fileHandle.getFile();
  } catch (e) {
    if (typeof cb === "function") cb({ error: e });
    return;
  }

  Papa.parse(file, {
    header: true,
    complete: (imported) => {
      const headerSig = JSON.stringify(Object.keys(imported.data[0]));
      switch (headerSig) {
        case IMPORT_TYPE.Vb_POI.SIGNATURE:
        case IMPORT_TYPE.V1_POI.SIGNATURE:
          if (typeof cb === "function") cb(IMPORT_TYPE.V1_POI.PARSER(imported));
          break;
        default:
          console.error("Invalid CSV file: Unknown header signature");
          console.error(headerSig);
          if (typeof cb === "function") cb(null);
          break;
      }
    }
  });
};
