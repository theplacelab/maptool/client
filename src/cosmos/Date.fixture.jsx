import { Input } from "src/components/Form";
import { FIELD_TYPE } from "src/components/Form";

export default (
  <form>
    <Input
      type={FIELD_TYPE.DATE}
      value={"Date"}
      label={"Date"}
      autoComplete="off"
    />
    <Input
      disabled={true}
      type={FIELD_TYPE.DATE}
      value={"Date Disabled"}
      label={"Date Disabled"}
      autoComplete="off"
    />
    <Input
      published={false}
      type={FIELD_TYPE.DATE}
      value={"Date Disabled"}
      label={"Date Not Published"}
      autoComplete="off"
    />
    <Input
      locked={true}
      type={FIELD_TYPE.DATE}
      value={"Date"}
      label={"Date Locked"}
      autoComplete="off"
    />
  </form>
);
