import { Input } from "src/components/Form";
import { FIELD_TYPE } from "src/components/Form";

const value = {
  lat: 40.7128,
  lng: 74.006
};

export default (
  <form>
    <Input type={FIELD_TYPE.COORDINATES} value={value} label={"Coordinates"} />
    <Input
      type={FIELD_TYPE.COORDINATES}
      disabled={true}
      value={value}
      label={"Disabled Coordinates"}
    />
    <Input
      type={FIELD_TYPE.COORDINATES}
      published={false}
      value={value}
      label={"Not Published Coordinates"}
    />
    <Input
      type={FIELD_TYPE.COORDINATES}
      locked={true}
      value={value}
      label={"Locked Coordinates"}
    />
  </form>
);
