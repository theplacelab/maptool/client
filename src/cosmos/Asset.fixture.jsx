import { Input } from "src/components/Form";
import { FIELD_TYPE } from "src/components/Form";

export default (
  <form>
    <Input
      type={FIELD_TYPE.ASSET}
      value={"Asset"}
      label={"Asset"}
      autoComplete="off"
    />
    <Input
      disabled={true}
      type={FIELD_TYPE.ASSET}
      value={"Asset Disabled"}
      label={"Asset Disabled"}
      autoComplete="off"
    />
    <Input
      published={false}
      type={FIELD_TYPE.ASSET}
      value={"Asset Disabled"}
      label={"Asset Not Published"}
      autoComplete="off"
    />
    <Input
      locked={true}
      type={FIELD_TYPE.ASSET}
      value={"Asset"}
      label={"Asset Locked"}
      autoComplete="off"
    />
  </form>
);
