import { Input } from "src/components/Form";
import { FIELD_TYPE } from "src/components/Form";

export default (
  <form>
    <Input
      type={FIELD_TYPE.TAGLIST}
      value={"cat,dog,moose,goose"}
      label={"TagList"}
      autoComplete="off"
    />
    <Input
      type={FIELD_TYPE.TAGLIST}
      value={null}
      label={"TagList Empty"}
      autoComplete="off"
    />
    <Input
      disabled={true}
      type={FIELD_TYPE.TAGLIST}
      value={"cat,dog,moose,goose"}
      label={"TagList Disabled"}
      autoComplete="off"
    />
    <Input
      published={false}
      type={FIELD_TYPE.TAGLIST}
      value={"cat,dog,moose,goose"}
      label={"TagList Not Published"}
      autoComplete="off"
    />
    <Input
      locked={true}
      type={FIELD_TYPE.TAGLIST}
      value={"cat,dog,moose,goose"}
      label={"TagList Locked"}
      autoComplete="off"
    />
  </form>
);
