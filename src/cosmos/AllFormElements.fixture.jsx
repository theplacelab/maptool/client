import { Input } from "src/components/Form";
import { FIELD_TYPE } from "src/components/Form";

const props = {
  disabled: false,
  published: true,
  markdown: false,
  searchable: false,
  locked: true
};
export default (
  <form>
    {Object.keys(FIELD_TYPE).map((key, idx) => (
      <Input
        key={idx}
        placeholder={FIELD_TYPE[key]}
        type={FIELD_TYPE[key]}
        label={FIELD_TYPE[key]}
        autoComplete="off"
        {...props}
      />
    ))}
  </form>
);
