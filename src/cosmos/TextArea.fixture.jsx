import { Input } from "src/components/Form";
import { FIELD_TYPE } from "src/components/Form";

export default (
  <form>
    <Input
      type={FIELD_TYPE.TEXTAREA}
      value={"*Here* **Is**  __Markdown__"}
      label={"TextArea"}
      autoComplete="off"
    />
    <Input
      disabled={true}
      type={FIELD_TYPE.TEXTAREA}
      value={"*Here* **Is**  __Markdown__"}
      label={"TextArea Disabled"}
      autoComplete="off"
    />
    <Input
      published={false}
      type={FIELD_TYPE.TEXTAREA}
      value={"*Here* **Is**  __Markdown__"}
      label={"TextArea Not Published"}
      autoComplete="off"
    />
    <Input
      locked={true}
      type={FIELD_TYPE.TEXTAREA}
      value={"*Here* **Is** Markdown"}
      label={"TextArea Locked"}
      autoComplete="off"
    />
    <Input
      markdown={true}
      type={FIELD_TYPE.TEXTAREA}
      value={"*Here* **Is**  __Markdown__"}
      label={"TextArea Markdown"}
      autoComplete="off"
    />
    <Input
      locked={true}
      markdown={true}
      type={FIELD_TYPE.TEXTAREA}
      value={"*Here* **Is**  __Markdown__"}
      label={"TextArea Markdown Locked"}
      autoComplete="off"
    />
  </form>
);
