import { createUseStyles } from "react-jss";

import { MdOutlineFormatColorText } from "react-icons/md";
import { FaImage } from "react-icons/fa";
import { IoMdPin } from "react-icons/io";
import { GoMultiSelect } from "react-icons/go";
import { BsCalendar2DateFill } from "react-icons/bs";
import { GrValidate, GrUserAdmin } from "react-icons/gr";
import {
  FaVideo,
  FaTags,
  FaUnlock,
  FaLock,
  FaBinoculars,
  FaEye,
  FaMarkdown,
  FaEyeSlash
} from "react-icons/fa";
import { RiCloudOffLine } from "react-icons/ri";
import { BsChevronBarDown, BsChevronBarUp } from "react-icons/bs";
import { HiArrowSmDown, HiArrowSmUp } from "react-icons/hi";
import { HiChevronRight } from "react-icons/hi";
import { FaChevronCircleRight, FaChevronCircleDown } from "react-icons/fa";
import { RiChatDeleteLine } from "react-icons/ri";
import { GrPrevious, GrNext } from "react-icons/gr";
import { FiMap } from "react-icons/fi";
import { RiTreasureMapFill, RiTreasureMapLine } from "react-icons/ri";
import { HiOutlineMap } from "react-icons/hi2";
import { MdOutlinePermMedia } from "react-icons/md";
import { FiUsers, FiSettings } from "react-icons/fi";
import { FcOk, FcHighPriority } from "react-icons/fc";
import { BsFiletypeHtml } from "react-icons/bs";

const icon = {
  COLOR: {
    VALID: <FcOk />,
    INVALID: <FcHighPriority />
  },
  SECTION: {
    SETTINGS: <FiSettings />,
    MAPS: <HiOutlineMap />,
    ASSETS: <MdOutlinePermMedia />,
    USERS: <FiUsers />,
    PREVIEW: <RiTreasureMapLine />
  },
  MAP_OUTLINE: <RiTreasureMapLine />,
  MAP_FILLED: <RiTreasureMapFill />,
  MAP_NOT_FOUND: <FiMap />,
  NEXT: <GrNext />,
  PREVIOUS: <GrPrevious />,
  INVALID: <RiChatDeleteLine />,
  CHEVRON_CIRCLE_RIGHT: <FaChevronCircleRight />,
  CHEVRON_CIRCLE_DOWN: <FaChevronCircleDown />,
  CHEVRON_RIGHT: <HiChevronRight />,
  CHEVRON_UP: <BsChevronBarUp />,
  CHEVRON_DOWN: <BsChevronBarDown />,
  ARROW_UP: <HiArrowSmUp />,
  ARROW_DOWN: <HiArrowSmDown />,
  OFFLINE: <RiCloudOffLine />,
  FIELD_STATUS: {
    SEARCHABLE: <FaBinoculars />,
    MARKDOWN: <FaMarkdown />,
    UNMARKDOWN: <BsFiletypeHtml />,
    PUBLISHED: <FaEye />,
    UNPUBLISHED: <FaEyeSlash />,
    LOCKED: <FaLock />,
    UNLOCKED: <FaUnlock />
  },
  USER: {
    ADMIN: <GrUserAdmin />,
    VALID: <GrValidate />
  },
  FIELD_TYPE: {
    TEXT: <MdOutlineFormatColorText tooltip="Tag List" />,
    SELECTION: <GoMultiSelect />,
    DATE: <BsCalendar2DateFill />,
    TAGLIST: <FaTags alt="Tag List" />,
    TEXTAREA: <MdOutlineFormatColorText />,
    COORDINATES: <IoMdPin />,
    IMAGE: <FaImage />,
    AUDIOVIDEO: <FaVideo />,
    ASSET: <FaVideo />
  }
};

const font = {
  display: "Staatliches"
};

const color = {
  accent: "#94a8a7",
  accent2: "#104b47",
  foreground: "#333333",
  background: "#ffffff",
  backgroundTint1: "#eeeeee",
  backgroundTint2: "#dddddd",
  backgroundTint3: "#f4f4f4",
  inputLabel: "#8e8c8c",
  poiMoveHighlight: "#bada55",
  warning: "#e6c635",
  ok: "#286078",
  online: "#72ee1d",
  offline: "red",
  alertError: "rgb(192 0 0)",
  alertOk: "rgb(29 135 95)",
  alertWarning: "rgb(255 181 4)",
  inlineError: "rgb(226 180 180)",
  inlineErrorText: "#656565",
  inlineOk: "rgb(137 201 163)",
  inlineOkText: "#656565",
  inlineInfo: "#ffc000",
  inlineInfoText: "#9a7400"
};
export { font, color, icon };

const useStyles = createUseStyles({
  adminContent: {
    padding: "1rem 1rem 3rem 1rem"
  },
  panel: {
    borderRadius: "0.3rem",
    margin: "1rem",
    padding: "0 1rem 0 1rem"
  },
  panelTitle: {
    fontWeight: 900,
    fontFamily: font.display,
    fontSize: "1.2rem",
    color: color.foreground,
    marginBottom: ".5rem"
  },
  inputSelect: {
    fontSize: "1.2rem",
    padding: ".5rem",
    width: "100%",
    margin: "8px 0 0 0",
    boxSizing: "border-box",
    borderRadius: "0.3rem",
    border: `.2px solid ${color.backgroundTint2}`,
    "&::placeholder": {
      textOverflow: "ellipsis !important",
      color: color.foreground,
      opacity: 0.2
    }
  },
  inputText: {
    padding: ".5rem",
    margin: "8px 0 0 0",
    fontSize: "1.2rem",
    width: "100%",
    boxSizing: "border-box",
    borderRadius: "0.3rem",
    border: `.2px solid ${color.backgroundTint2}`,
    "&::placeholder": {
      textOverflow: "ellipsis !important",
      color: color.foreground,
      opacity: 0.2
    }
  },
  inputTextArea: {
    minHeight: "10rem",
    position: "relative",
    top: ".5rem",
    fontSize: "1.2rem",
    width: "100%",
    margin: "0",
    boxSizing: "border-box",
    borderRadius: "0.3rem",
    border: `.2px solid ${color.backgroundTint2}`,
    "&::placeholder": {
      textOverflow: "ellipsis !important",
      color: color.foreground,
      opacity: 0.2
    }
  },
  inputSearch: {
    fontSize: "1rem",
    width: "100%",
    margin: "8px 0",
    boxSizing: "border-box",
    borderRadius: "0.3rem",
    border: `.2px solid ${color.backgroundTint2}`,
    "&::placeholder": {
      textOverflow: "ellipsis !important",
      color: color.foreground,
      opacity: 0.2
    }
  },
  inputTextLabel: {
    position: "absolute",
    top: "-.4rem",
    fontSize: ".8rem",
    color: color.inputLabel
  },
  inputLabel: {
    fontSize: ".8rem",
    color: color.inputLabel
  },
  inputCheckboxLabel: {
    marginLeft: ".5rem",
    top: "-.4rem",
    fontSize: ".8rem",
    color: color.inputLabel
  },
  inputButton: {
    padding: "1rem",
    userSelect: "none",
    fontFamily: font.display,
    fontWeight: 900,
    fontSize: "1.2rem",
    width: "100%",
    margin: "0",
    color: color.accent2,
    backgroundColor: color.accent,
    opacity: 0.8,
    textAlign: "center",
    border: "0px",
    borderRadius: "0.3rem",
    boxSizing: "border-box",
    transition: "opacity .5s",
    "&:hover": {
      opacity: 1.0,
      transition: "opacity .5s"
    }
  },
  inputCustomBorder: {
    border: `1px solid ${color.backgroundTint2}`,
    padding: "1rem",
    borderRadius: "0.3rem"
  },
  container: {
    display: "flex",
    height: "100vh",
    background: color.backgroundTint1
  },
  title: {
    fontWeight: 900,
    fontFamily: font.display,
    fontSize: "2.3rem",
    color: color.accent
  },
  flexRow: {
    display: "flex",
    flexDirection: "row"
  },
  horizontalRule: {
    margin: "2rem 0 2rem 0",
    borderBottom: `1px solid ${color.backgroundTint2}`
  },
  smallButton: {
    padding: "1rem",
    userSelect: "none",
    fontFamily: font.display,
    fontWeight: 900,
    color: color.accent2,
    backgroundColor: color.accent,
    opacity: 0.8,
    textAlign: "center",
    border: "0px",
    borderRadius: "0.3rem",
    boxSizing: "border-box",
    transition: "opacity .5s",
    minWidth: 0,
    height: "1rem",
    fontSize: "1rem",
    lineHeight: 0,
    margin: ".2rem",
    "&:hover": {
      opacity: 1.0,
      transition: "opacity .5s"
    }
  },
  upperRightCta: {
    display: "flex",
    position: "absolute",
    top: "1.25rem",
    right: "1rem"
  },
  tab: {
    color: color.foreground,
    margin: "0 0 .5rem 0",
    padding: ".5rem 1rem .5rem 1rem",
    borderBottom: `3px solid ${color.backgroundTint1}`
  },
  tabSelected: {
    fontWeight: 900,
    color: color.accent,
    margin: "0 0 .5rem 0",
    borderBottom: `3px solid ${color.accent}`,
    background: color.backgroundTint1,
    padding: ".5rem 1rem .5rem 1rem",
    borderRadius: "0.3rem 0.3rem 0 0"
  },
  tagList: {
    position: "relative",
    top: ".5rem",
    display: "flex",
    flexDirection: "row"
  },
  tag: {
    backgroundColor: color.accent,
    borderRadius: "0.3rem",
    color: color.background,
    minWidth: "3rem",
    textAlign: "center",
    padding: ".2rem",
    margin: ".2rem"
  }
});
export default useStyles;
