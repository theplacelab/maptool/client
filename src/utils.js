import validator from "validator";

export const JSONParse = (data) => {
  let parsed = null;
  try {
    parsed = data ? JSON.parse(data) : null;
  } catch {}
  return parsed;
};

export const validatedId = (id) =>
  id == null
    ? null
    : validator.isUUID(id)
    ? id
    : Number.isNaN(Number(id))
    ? null
    : Number(id);

export const isValidMapStyle = (mapStyle) => {
  if (typeof mapStyle === "string" && mapStyle?.includes("http")) {
    return fetch(mapStyle, {
      method: "GET",
      headers: {
        Accept: "application/json, application/xml, text/plain, text/html, *.*"
      }
    })
      .then((response) => response.json())
      .then((data) => data != null && data?.version);
  } else {
    return mapStyle != null && mapStyle?.version;
  }
};

export const fallbackCopyTextToClipboard = (text) => {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand("copy");
    var msg = successful ? "successful" : "unsuccessful";
    console.log("Fallback: Copying text command was " + msg);
  } catch (err) {
    console.error("Fallback: Oops, unable to copy", err);
  }

  document.body.removeChild(textArea);
};

export const copyTextToClipboard = (text) => {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(
    () => {
      //console.log("Async: Copying to clipboard was successful!");
    },
    (err) => {
      console.error("Async: Could not copy text: ", err);
    }
  );
};
