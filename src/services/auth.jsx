import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { color } from "../styles.jsx";

// API /////////////////
export const authApi = createApi({
  reducerPath: "authApi",
  baseQuery: fetchBaseQuery({
    baseUrl: window._env_.VITE_AUTH_SERVICE,
    prepareHeaders: (headers, { getState }) => {
      headers.set("Content-type", "application/json; charset=UTF-8");
      const authToken = getState().auth.token.auth;
      if (authToken && !headers.get("Authorization"))
        headers.set("Authorization", `Bearer ${authToken}`);
      return headers;
    }
  }),
  tagTypes: ["Auth", "Users"],
  endpoints: (build) => ({
    login: build.mutation({
      query: (payload) => ({
        url: "login",
        method: "POST",
        body: payload
      }),
      invalidatesTags: ["Auth"]
    }),
    changePassword: build.mutation({
      query: (payload) => ({
        url: `user/changepass`,
        method: "POST",
        body: payload
      }),
      invalidatesTags: ["Users"]
    }),
    createUser: build.mutation({
      query: (payload) => ({
        url: "user",
        method: "PUT",
        body: payload
      }),
      invalidatesTags: ["Users"]
    }),
    updateUser: build.mutation({
      query: (payload) => ({
        url: `user/${payload.id}`,
        method: "PATCH",
        body: payload
      }),
      invalidatesTags: ["Users"]
    }),
    deleteUser: build.mutation({
      query: (payload) => ({
        url: `user/${payload.id}`,
        method: "DELETE",
        body: payload
      }),
      invalidatesTags: ["Users"]
    }),
    getUsers: build.query({
      query: (payload) => ({
        url: "users",
        method: "GET",
        body: payload
      }),
      providesTags: ["Users"]
    }),
    getUserById: build.query({
      query: (id) => ({
        url: `user/${id}`,
        method: "GET"
      }),
      providesTags: ["Users"]
    }),
    reauthorize: build.mutation({
      query: (refreshToken) => {
        return {
          url: "reauthorize",
          method: "POST",
          headers: {
            Authorization: `Bearer ${refreshToken}`
          }
        };
      },
      invalidatesTags: ["Auth"]
    })
  })
});

// Additional Helpers /////////////////
const headers = {
  Accept: "application/json, application/xml, text/plain, text/html, *.*",
  "Content-Type": "application/json; charset=utf-8"
};
export const resetUser = async (email) => {
  try {
    const response = await fetch(
      `${window._env_.VITE_AUTH_SERVICE}/user/reset`,
      {
        method: "POST",
        headers,
        body: JSON.stringify({ email })
      }
    );
    if (response.status === 410) {
      return {
        code: response.status,
        text: `No such user: ${email.toLowerCase().trim()}`,
        color: color.alertWarning
      };
    } else {
      return {
        code: response.status,
        text: "Your account has been reset, please check your email to continue",
        color: color.alertOk
      };
    }
  } catch (err) {
    return {
      code: null,
      text: "Service is unreachable",
      color: color.alertError
    };
  }
};

export const validate = async (token) => {
  try {
    const response = await fetch(
      `${window._env_.VITE_AUTH_SERVICE}/user/validate`,
      {
        method: "POST",
        headers,
        body: JSON.stringify({ token })
      }
    );
    if (response.status === 200) {
      return { code: response.status, validation: "ok" };
    } else {
      return { code: response.status, validation: "expired" };
    }
  } catch (err) {
    return { code: null, validation: "server unreachable" };
  }
};

export const up = async () => {
  try {
    const response = await fetch(`${window._env_.VITE_AUTH_SERVICE}/up`, {
      method: "GET",
      headers
    });
    return response.ok;
  } catch (err) {
    return false;
  }
};

export const {
  useLoginMutation,
  useGetUsersQuery,
  useGetUserByIdQuery,
  useReauthorizeMutation,
  useCreateUserMutation,
  useUpdateUserMutation,
  useDeleteUserMutation,
  useChangePasswordMutation
} = authApi;
