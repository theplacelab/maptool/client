import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const assetApi = createApi({
  reducerPath: "assetApi",
  baseQuery: fetchBaseQuery({
    baseUrl: window._env_.VITE_ASSET_SERVICE,
    prepareHeaders: (headers, { getState }) => {
      headers.set("Content-type", "application/json; charset=UTF-8");
      const authToken = getState().auth.token.auth;
      if (authToken && !headers.get("Authorization"))
        headers.set("Authorization", `Bearer ${authToken}`);
      return headers;
    }
  }),
  tagTypes: ["Assets"],
  endpoints: (builder) => ({
    getAssets: builder.query({
      query: () => ({
        method: "GET",
        url: "assets"
      }),
      providesTags: ["Assets"]
    }),
    getAssetById: builder.query({
      query: (id) => ({
        method: "GET",
        url: `asset/${id}`
      }),
      providesTags: ["Assets"]
    }),
    updateAsset: builder.mutation({
      query: ({ mapId, poiData }) => ({
        url: `asset/${mapId}`,
        method: "PATCH",
        body: poiData
      }),
      invalidatesTags: ["Assets"]
    })
  })
});

export const up = async () => {
  try {
    const response = await fetch(`${window._env_.VITE_ASSET_SERVICE}/up`, {
      method: "GET",
      headers: {
        Accept: "application/json, application/xml, text/plain, text/html, *.*",
        "Content-Type": "application/json; charset=utf-8"
      }
    });
    return response.ok;
  } catch (err) {
    return false;
  }
};

export const {
  useGetAssetsQuery,
  useGetAssetByIdQuery,
  useUpdateAssetMutation
} = assetApi;
