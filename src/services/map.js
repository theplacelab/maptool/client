import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// API //////////////////
export const mapApi = createApi({
  reducerPath: "mapApi",
  baseQuery: fetchBaseQuery({
    baseUrl: window._env_.VITE_MAP_SERVICE,
    prepareHeaders: (headers, { getState }) => {
      headers.set("Content-type", "application/json; charset=UTF-8");
      const authToken = getState().auth.token.auth;
      if (authToken && !headers.get("Authorization"))
        headers.set("Authorization", `Bearer ${authToken}`);
      return headers;
    }
  }),
  tagTypes: ["Maps", "Poi", "Settings"],
  endpoints: (builder) => ({
    createMap: builder.mutation({
      query: (payload) => ({
        url: "map",
        method: "PUT",
        body: payload
      }),
      invalidatesTags: ["Maps"]
    }),
    getMaps: builder.query({
      query: () => ({
        method: "GET",
        url: "maps"
      }),
      providesTags: ["Maps"]
    }),
    updateMapOrder: builder.mutation({
      query: (body) => ({
        method: "POST",
        url: `maps/order`,
        body
      }),
      invalidatesTags: ["Maps"]
    }),
    getMapById: builder.query({
      query: (id) => ({
        method: "GET",
        url: `map/${id}`
      }),
      providesTags: ["Maps"]
    }),
    getGlobalSettings: builder.query({
      query: (id) => ({
        method: "GET",
        url: `settings`
      }),
      providesTags: ["Settings"]
    }),
    updateGlobalSettings: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `settings`,
        body
      }),
      invalidatesTags: ["Settings"]
    }),
    getMapSettingsById: builder.query({
      query: (id) => ({
        method: "GET",
        url: `settings/map/${id}`
      }),
      providesTags: ["Maps"]
    }),
    updateMap: builder.mutation({
      query: (body) => ({
        method: "PATCH",
        url: `map/${body.id}`,
        body
      }),
      invalidatesTags: ["Maps"]
    }),
    deleteMap: builder.mutation({
      query: (mapId) => ({
        url: `map/${mapId}`,
        method: "DELETE",
        body: { mapId }
      }),
      invalidatesTags: ["Maps"]
    }),
    getPoi: builder.query({
      query: (mapId) => ({
        method: "GET",
        url: `map/${mapId}/poi`
      }),
      providesTags: ["Poi"]
    }),
    getPoiById: builder.query({
      query: ({ mapId, poiId }) => ({
        method: "GET",
        url: `map/${mapId}/poi/${poiId}`
      }),
      providesTags: ["Poi"]
    }),
    getPoiStyle: builder.query({
      query: (mapId) => ({
        method: "GET",
        url: `settings/poi-style`
      }),
      providesTags: ["Settings"]
    }),
    getPoiDataFormat: builder.query({
      query: (mapId) => ({
        method: "GET",
        url: `settings/poi-data-format`
      }),
      providesTags: ["Settings"]
    }),

    getPoiField: builder.query({
      query: (mapId) => ({
        method: "GET",
        url: `settings/map/${mapId}/poi/config`
      }),
      providesTags: ["PoiField"]
    }),
    getPoiFieldById: builder.query({
      query: ({ mapId, configId }) => ({
        method: "GET",
        url: `settings/map/${mapId}/poi/config/${configId}`
      }),
      providesTags: ["PoiField"]
    }),
    deletePoiField: builder.mutation({
      query: ({ mapId, configId }) => ({
        method: "DELETE",
        url: `settings/map/${mapId}/poi/config/${configId}`
      }),
      providesTags: ["PoiField"]
    }),
    createPoiField: builder.mutation({
      query: ({ mapId, config }) => ({
        url: `settings/map/${mapId}/poi/config`,
        method: "PUT",
        body: config
      }),
      invalidatesTags: ["PoiField"]
    }),
    updatePoiField: builder.mutation({
      query: ({ mapId, data }) => ({
        url: `settings/map/${mapId}/poi/config/${data.id}`,
        method: "PATCH",
        body: data
      }),
      invalidatesTags: ["PoiField"]
    }),
    updatePoiFieldOrder: builder.mutation({
      query: ({ mapId, order }) => ({
        url: `settings/map/${mapId}/poi/config/order`,
        method: "POST",
        body: order
      }),
      invalidatesTags: ["PoiField"]
    }),
    createPoi: builder.mutation({
      query: ({ mapId, newPoi }) => ({
        url: `map/${mapId}/poi`,
        method: "PUT",
        body: newPoi
      }),
      invalidatesTags: ["Poi"]
    }),
    updatePoiOrder: builder.mutation({
      query: ({ mapId, order }) => ({
        url: `map/${mapId}/poi/order`,
        method: "POST",
        body: order
      }),
      invalidatesTags: ["Poi"]
    }),
    deletePoi: builder.mutation({
      query: ({ mapId, poiId }) => ({
        url: `map/${mapId}/poi/${poiId}`,
        method: "DELETE"
      }),
      invalidatesTags: ["Poi"]
    }),
    updatePoi: builder.mutation({
      query: ({ mapId, poiData }) => ({
        url: `map/${mapId}/poi/${poiData.id}`,
        method: "PATCH",
        body: poiData
      }),
      invalidatesTags: ["Poi"]
    })
  })
});

// Additional Helpers //
export const up = async () => {
  try {
    const response = await fetch(`${window._env_.VITE_MAP_SERVICE}/up`, {
      method: "GET",
      headers: {
        Accept: "application/json, application/xml, text/plain, text/html, *.*",
        "Content-Type": "application/json; charset=utf-8"
      }
    });
    return response.ok;
  } catch (err) {
    return false;
  }
};

export const {
  useGetGlobalSettingsQuery,
  useDeleteMapMutation,
  useCreateMapMutation,
  useGetMapsQuery,
  useUpdateMapMutation,
  useGetMapByIdQuery,
  useUpdateMapOrderMutation,
  useUpdateGlobalSettingsMutation,
  useGetMapSettingsByIdQuery,
  useGetPoiQuery,
  useCreatePoiMutation,
  useCreatePoiFieldMutation,
  useDeletePoiMutation,
  useUpdatePoiMutation,
  useUpdatePoiOrderMutation,
  useGetPoiDataFormatQuery,
  useGetPoiStyleQuery,
  useGetPoiFieldQuery,
  useDeletePoiFieldMutation,
  useGetPoiFieldByIdQuery,
  useUpdatePoiFieldMutation,
  useGetPoiByIdQuery,
  useUpdatePoiFieldOrderMutation
} = mapApi;
