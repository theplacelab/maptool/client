import React from "react";
import { color } from "src/styles.jsx";
import { SpinnerCircular } from "spinners-react";
export default function Spinner({ message = "" }) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
        textAlign: "center"
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
          textAlign: "center",
          marginLeft: "2.3rem"
        }}
      >
        <SpinnerCircular
          size={20}
          thickness={100}
          speed={100}
          color={color.accent}
          secondaryColor={color.backgroundTint1}
        />{" "}
        <div style={{ color: color.backgroundTint2, marginLeft: ".5rem" }}>
          Loading...
        </div>
      </div>
    </div>
  );
}
