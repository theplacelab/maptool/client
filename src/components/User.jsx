import React, { useState } from "react";
import { color, icon } from "src/styles.jsx";
import { createUseStyles } from "react-jss";
import { useNavigate } from "react-router-dom";
import { IconContext } from "react-icons";
import WatchForOutsideClick from "src/components/WatchForOutsideClick";
import { useGetUserByIdQuery } from "src/services/auth";

const ChevronIcon = () => {
  return (
    <IconContext.Provider
      value={{
        color: color.accent,
        size: "1.5rem"
      }}
    >
      <span style={{ position: "relative", top: ".3rem", left: "-.23rem" }}>
        {icon.CHEVRON_RIGHT}
      </span>
    </IconContext.Provider>
  );
};

export default function User({ onLogout, id }) {
  const navigate = useNavigate();
  const { data: userInfo } = useGetUserByIdQuery(id);
  const [expanded, setExpanded] = useState(false);
  const size = 40;
  const classes = useStyles();
  const styles = {
    menu: {
      position: "absolute",
      top: "-85px",
      height: "110px",
      background: color.backgroundTint2,
      width: "100%",
      borderRadius: "0.3rem",
      padding: "0.6rem",
      boxSizing: "border-box"
    },
    container: {
      userSelect: "none",
      position: "relative",
      background: color.backgroundTint3,
      borderRadius: 20,
      width: expanded ? `90%` : "5rem",
      display: "flex",
      alignItems: "center"
    },
    avatar: {
      position: "relative",
      minHeight: `${size}px`,
      minWidth: `${size}px`,
      borderRadius: 200,
      backgroundPosition: "center",
      backgroundSize: `${size}px`,
      backgroundRepeat: "no-repeat",
      border: `3px solid ${color.backgroundTint3}`,
      left: -2
    },
    name: {
      position: "relative",
      zIndex: 1,
      color: color.foreground,
      marginLeft: ".5rem",
      fontWeight: 400,
      fontSize: "0.7rem",
      lineHeight: "1.4rem",
      textOverflow: "ellipsis",
      overflow: "hidden",
      width: "7.4rem",
      whiteSpace: "nowrap"
    }
  };
  const avatarIcon = userInfo?.avatar
    ? {
        ...styles.avatar,
        backgroundImage: `url(${userInfo.avatar})`,
        color: "#FFFFFF00"
      }
    : {
        ...styles.avatar,
        background: color.accent,
        color: "white",
        textAlign: "center",
        fontSize: "1rem",
        lineHeight: "2.4rem",
        fontWeight: 900
      };
  if (!userInfo) return null;
  return (
    <WatchForOutsideClick onOutsideClick={() => setExpanded(false)}>
      <div onClick={() => setExpanded(!expanded)} style={styles.container}>
        {expanded && (
          <div style={styles.menu}>
            <div
              onClick={() => {
                navigate(`/maptool/user/${userInfo.uuid}`);
              }}
              className={classes.menuItem}
              style={{
                borderBottom: `1px solid ${color.backgroundTint1}`
              }}
            >
              Profile
            </div>
            <div onClick={onLogout} className={classes.menuItem}>
              Sign Out
            </div>
          </div>
        )}
        <div style={avatarIcon}>{userInfo?.name.charAt(0)}</div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div style={styles.name}>{expanded ? userInfo.email : ""}</div>
          <div style={styles.name}>
            {expanded ? userInfo.name : <ChevronIcon />}
          </div>
        </div>
      </div>
    </WatchForOutsideClick>
  );
}
const useStyles = createUseStyles({
  menuItem: {
    padding: ".5rem 0 .6rem .5rem",
    "&:hover": {
      background: color.accent
    }
  }
});
