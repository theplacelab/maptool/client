import React from "react";
import { color, font } from "src/styles.jsx";
import WatchForOutsideClick from "src/components/WatchForOutsideClick";
import { Button } from "src/components";

export default function Dialog({
  isVisible,
  title,
  message,
  confirmOnly = false,
  cancelLabel = "Cancel",
  confirmLabel = "OK",
  onConfirm,
  onCancel
}) {
  const onCancelAction = () => {
    if (typeof onCancel === "function") onCancel();
  };

  const onConfirmAction = () => {
    if (typeof onConfirm === "function") onConfirm();
  };

  if (!isVisible) return null;

  return (
    <div style={styles.container}>
      <div style={styles.inner}>
        <WatchForOutsideClick onOutsideClick={onCancelAction}>
          {title && <div style={styles.title}>{title}</div>}
          <div style={{ display: "flex", height: "10rem" }}>
            <div style={styles.message}>{message}</div>
          </div>
          <div style={{ height: "2rem", display: "flex", margin: "auto" }}>
            <div style={{ display: "flex", margin: "auto" }}>
              {!confirmOnly && (
                <Button
                  style={{ marginRight: ".5rem" }}
                  label={confirmLabel}
                  onClick={onConfirmAction}
                />
              )}
              <div>
                <Button label={cancelLabel} onClick={onCancelAction} />
              </div>
            </div>
          </div>
        </WatchForOutsideClick>
      </div>
    </div>
  );
}

const styles = {
  container: {
    position: "absolute",
    background: "rgb(255 255 255 / 80%)",
    height: "100vh",
    zIndex: 10,
    width: "100vw",
    display: "flex",
    top: 0,
    left: 0
  },
  inner: {
    display: "flex",
    borderRadius: ".2rem",
    flexDirection: "column",
    boxSizing: "border-box",
    padding: "2rem",
    margin: "auto",
    fontWeight: 900,
    background: color.backgroundTint1,
    height: "20rem",
    minHeight: "20rem",
    minWidth: "30rem",
    width: "30rem",
    maxWidth: "75%",
    maxHeight: "75%",
    boxShadow: "4px 4px 6px 0px #888888"
  },
  title: {
    fontFamily: font.display,
    fontSize: "1.5rem",
    lineHeight: "1.3rem",
    color: color.accent,
    fontWeight: 900,
    height: "1rem",
    background: color.backgroundTint3,
    borderRadius: ".2rem",
    padding: "1rem"
  },
  message: {
    fontWeight: 400,
    textAlign: "center",
    margin: "auto",
    overflow: "scroll",
    padding: "0 0 1rem 0"
  }
};
