import React, { useState, useEffect } from "react";
import { color, font } from "src/styles.jsx";
import { CSS } from "@dnd-kit/utilities";
import {
  SortableContext,
  verticalListSortingStrategy
} from "@dnd-kit/sortable";
import { useSortable } from "@dnd-kit/sortable";
import { InlineSpinner } from "src/components";

const SortableItem = (props) => {
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id: props.id });
  return (
    <div
      ref={setNodeRef}
      style={{
        transform: CSS.Translate.toString(transform),
        transition
      }}
      {...attributes}
      {...listeners}
    >
      {props.children}
    </div>
  );
};

const SortableList = ({ data, itemActions, collectionActions }) => {
  const [items, setItems] = useState();
  const [timerRunning, setTimerRunning] = useState(true);
  useEffect(() => {
    setItems(data);
    if (timerRunning)
      setTimeout(() => {
        setTimerRunning(false);
      }, 1000);
  }, [timerRunning, data]);

  if (!items && timerRunning) return <InlineSpinner />;

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column"
      }}
    >
      <SortableContext
        items={items ? items : []}
        strategy={verticalListSortingStrategy}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div style={{ flexGrow: 1 }}></div>
          <div style={{ alignItems: "right", marginRight: ".5rem" }}>
            {collectionActions}
          </div>
        </div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          {items?.map((item) => (
            <div key={item.id} style={styles.dragRow} data-id={item.id}>
              <SortableItem key={item.id} id={item.id}>
                <div style={styles.dragHandleContainer}>
                  <div style={styles.dragHandle}>::</div>
                  <div style={styles.item}>{item.label}</div>
                </div>
              </SortableItem>
              <div style={styles.itemActions}>{itemActions}</div>
            </div>
          ))}
        </div>
      </SortableContext>
    </div>
  );
};
export default SortableList;

const styles = {
  button: { minWidth: "5rem", maxWidth: "5rem" },
  dragHandleContainer: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  dragRow: {
    display: "flex",
    flexDirection: "row",
    border: `1px solid ${color.backgroundTint2}`,
    borderRadius: ".5rem 0 0 .5rem",
    height: "2.5rem",
    margin: ".2rem"
  },
  dragHandle: {
    marginLeft: ".5rem",
    paddingRight: ".5rem",
    borderRight: `1px solid ${color.backgroundTint2}`
  },
  header: {
    display: "flex",
    minWidth: "15rem",
    background: color.backgroundTint1,
    height: "2rem",
    textAlign: "left",
    paddingLeft: ".5rem",
    fontFamily: font.display,
    fontSize: "1.3rem",
    color: color.accent,
    alignItems: "center"
  },
  item: {
    flexGrow: 1,
    display: "flex",
    textAlign: "left",
    paddingLeft: ".5rem",
    fontSize: "1rem",
    color: color.accent,
    alignItems: "center",
    height: "2.5rem"
  },
  itemActions: {
    display: "flex",
    flexGrow: 1,
    justifyContent: "flex-end"
  }
};
