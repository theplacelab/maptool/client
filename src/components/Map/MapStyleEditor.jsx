import React, { useState } from "react";
import { default as useStyles, color } from "src/styles.jsx";
import { JSONEditor, TabSet, Tab } from "src/components";
import { Input, FIELD_TYPE } from "src/components/Form";
import MapDisplay from "src/components/Map/MapDisplay";
import { setAlert } from "src/redux/slice/alert";
import { useDispatch } from "react-redux";

const MapStyleEditor = ({
  setFormValues,
  formValues,
  setMessage,
  mapConfig,
  onChange
}) => {
  const dispatch = useDispatch();
  const onMapStyleUrl = async (e) => {
    const url = e.target.value;
    const onFail = () => {
      clearMapStyleUrl();
      onChange({ target: { value: {}, id: "style_json" } });
      setMessage({
        text: `Invalid style URL!`,
        background: color.inlineError,
        color: color.inlineErrorText
      });
    };
    const response = await fetch(url, {
      method: "GET",
      headers: {
        Accept: "application/json, application/xml, text/plain, text/html, *.*"
      }
    });
    if (response.status === 200) {
      const style_json = await response.json();
      if (style_json.version) {
        onChange({
          target: { value: JSON.stringify(style_json), id: "style_json" }
        });
        clearMapStyleUrl();
      } else {
        onFail();
      }
    } else {
      onFail();
    }
  };
  const classes = useStyles();
  const [mapStyleUrl, setMapStyleUrl] = useState("");
  const clearMapStyleUrl = () => setMapStyleUrl("");
  return (
    <div>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <div style={{ flexGrow: 1 }}>
          <Input
            style={{ background: "#bbbbbb26", marginTop: "-17px" }}
            className={classes.inputText}
            type={FIELD_TYPE.TEXT}
            onChange={(e) => setMapStyleUrl(e.target.value)}
            onBlur={onMapStyleUrl}
            value={mapStyleUrl}
            placeholder="Paste Style URL Here"
          />
        </div>
        <div style={{ position: "relative", marginLeft: "1rem" }}>
          [
          <a
            target="_blank"
            href="https://docs.themaptool.com/#/user-guide?id=map-style-json"
            rel="noreferrer"
          >
            help
          </a>
          ]
        </div>
      </div>
      <TabSet initialIndex={0} style={{ margin: "0 1rem 0 1rem" }}>
        <Tab title={"Map"}>
          <div>
            {formValues.settings && (
              <MapDisplay
                style={styles.MapDisplay}
                config={mapConfig}
                mapContextMenu={[
                  {
                    label: "Set Initials",
                    action: ({ map, coordinates }) => {
                      const value = map.getZoom().toFixed(4);
                      const center = map.getCenter();
                      setFormValues((formValues) => {
                        return {
                          ...formValues,
                          settings: {
                            ...formValues.settings,
                            zoomInitial: value,
                            centerInitial: JSON.stringify(center)
                          }
                        };
                      });
                      dispatch(
                        setAlert({
                          text: `Initial view set`,
                          color: color.accent2,
                          expiresAfter: 2000
                        })
                      );
                    }
                  },
                  {
                    label: "Set Initial Zoom",
                    action: ({ map }) => {
                      const value = map.getZoom().toFixed(4);
                      setFormValues((formValues) => {
                        return {
                          ...formValues,
                          settings: {
                            ...formValues.settings,
                            zoomInitial: value
                          }
                        };
                      });
                      dispatch(
                        setAlert({
                          text: `Initial zoom set to ${value}`,
                          color: color.accent2,
                          expiresAfter: 2000
                        })
                      );
                    }
                  },
                  {
                    label: "Set Min Zoom",
                    action: ({ map }) => {
                      const value = map.getZoom().toFixed(4);
                      setFormValues((formValues) => {
                        return {
                          ...formValues,
                          settings: {
                            ...formValues.settings,
                            zoomMin: value
                          }
                        };
                      });
                      dispatch(
                        setAlert({
                          text: `Min zoom set to ${value}`,
                          color: color.accent2,
                          expiresAfter: 2000
                        })
                      );
                    }
                  },
                  {
                    label: "Set Max Zoom",
                    action: ({ map }) => {
                      const value = map.getZoom().toFixed(4);
                      setFormValues((formValues) => {
                        return {
                          ...formValues,
                          settings: {
                            ...formValues.settings,
                            zoomMax: value
                          }
                        };
                      });
                      dispatch(
                        setAlert({
                          text: `Max zoom set to ${value}`,
                          color: color.accent2,
                          expiresAfter: 2000
                        })
                      );
                    }
                  },
                  {
                    label: "Set Initial Center",
                    action: ({ map, coordinates }) => {
                      map.flyTo({ center: coordinates });
                      setFormValues((formValues) => {
                        return {
                          ...formValues,
                          settings: {
                            ...formValues.settings,
                            centerInitial: JSON.stringify(coordinates)
                          }
                        };
                      });
                      dispatch(
                        setAlert({
                          text: `Initial center set to ${coordinates.lat}, ${coordinates.lng}`,
                          color: color.accent2,
                          expiresAfter: 2000
                        })
                      );
                    }
                  }
                ]}
              />
            )}
          </div>
        </Tab>
        <Tab title={"Raw"}>
          <Input
            style={{ height: "400px", marginTop: "-10px" }}
            label=""
            id="style_json"
            type={FIELD_TYPE.TEXTAREA}
            autoComplete="style_json"
            onChange={(e) => {
              const data = JSON.parse(e.target.value);
              onChange({
                target: {
                  value: data,
                  id: "style_json"
                }
              });
              setMapStyleUrl("");
            }}
            value={
              formValues.style_json
                ? JSON.stringify(formValues.style_json, null, 2)
                : ""
            }
            placeholder=""
          />
        </Tab>
        <Tab title={"JSON"}>
          <JSONEditor
            style={{ height: "400px" }}
            value={formValues.style_json}
            onChange={(data) => {
              onChange({
                target: {
                  value: data,
                  id: "style_json"
                }
              });
            }}
          />
        </Tab>
      </TabSet>
    </div>
  );
};
export default MapStyleEditor;

const styles = {
  MapDisplay: {
    position: "relative",
    margin: "auto",
    width: "100%",
    height: "400px",
    border: "1px solid grey"
  }
};
