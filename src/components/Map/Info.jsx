import React from "react";
import { color } from "src/styles";

export const Info = ({ lat, lng, zoom }) => (
  <div
    style={{
      backgroundColor: color.accent,
      color: color.accent2,
      padding: "6px 12px",
      fontFamily: "monospace",
      zIndex: 1,
      position: "absolute",
      top: "2rem",
      left: 6,
      margin: "12px",
      borderRadius: "4px"
    }}
  >
    Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
  </div>
);
