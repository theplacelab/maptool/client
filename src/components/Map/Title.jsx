import React from "react";
import { color } from "src/styles";

export const Title = ({ title }) => (
  <div
    style={{
      backgroundColor: color.accent,
      color: color.accent2,
      padding: "6px 12px",
      fontFamily: "monospace",
      zIndex: 1,
      position: "absolute",
      top: 0,
      left: 6,
      margin: "12px",
      borderRadius: "4px"
    }}
  >
    {title}
  </div>
);
