import React, { useEffect } from "react";
import { WatchForOutsideClick } from "src/components";
import {
  useGetPoiByIdQuery,
  useGetPoiDataFormatQuery,
  useGetPoiFieldQuery
} from "src/services/map";
import { color } from "src/styles.jsx";
import { Input, FIELD_TYPE } from "src/components/Form";

const PoiDetail = ({ mapId, id, data, onClose }) => {
  console.log(data);
  const [editField, setEditField] = React.useState(null);
  const { data: poiFields } = useGetPoiFieldQuery(mapId);
  const { data: pd } = useGetPoiByIdQuery({ mapId, poiId: id });
  const { data: poiDataFormat } = useGetPoiDataFormatQuery();
  const [formFields, setFormFields] = React.useState(null);
  const poiData = pd ? pd[0] : null;

  const Row = ({ label, value, field }) => (
    <div onClick={() => setEditField("coordinates")} style={styles.row}>
      <div style={styles.header}>{label}</div>
      <div>{value}</div>
    </div>
  );

  const ClickToEdit = ({ label, value, onClose }) => (
    <WatchForOutsideClick onOutsideClick={onClose}>
      <div onClick={() => setEditField(label)} style={styles.row}>
        <div contentEditable={editField === label}>{value}</div>
      </div>
    </WatchForOutsideClick>
  );

  const onChange = (e) => {
    const newFormFields = [...formFields];
    newFormFields.find((f) => String(f.id) === String(e.target.id)).value =
      e.target.value;
    setFormFields(newFormFields);
    console.log(newFormFields);
  };

  useEffect(() => {
    if (data) {
      setFormFields(data);
    } else if (poiDataFormat && poiData && poiFields && poiFields[0]) {
      let f = structuredClone(poiFields);
      f.unshift({
        id: "coordinates",
        label: "Coordinates",
        value: poiData.coordinates,
        poi_data_format_id: poiDataFormat.find(
          (f) => f.slug.toLowerCase() === FIELD_TYPE.COORDINATES
        ).id
      });
      f.unshift({
        id: "title",
        label: "",
        value: poiData.title,
        poi_data_format_id: poiDataFormat.find(
          (f) => f.slug.toLowerCase() === FIELD_TYPE.TEXT
        ).id
      });
      setFormFields(f);
    }
  }, [data, poiData, poiFields, poiDataFormat]);

  if (formFields && !formFields.map) return null;

  return (
    <WatchForOutsideClick onOutsideClick={onClose}>
      <div style={styles.container}>
        {formFields?.map((field, idx) => {
          return (
            <div key={idx}>
              <Input
                id={field.id}
                onChange={onChange}
                published={field.is_published}
                markdown={field.is_markdown}
                locked={field.is_locked}
                label={field.label}
                type={
                  FIELD_TYPE[
                    poiDataFormat?.find(
                      (f) => f.id === field.poi_data_format_id
                    )?.slug
                  ]
                }
                value={field.value}
              />
            </div>
          );
        })}
        <div style={{ fontSize: ".7rem" }}>
          <div style={styles.hr} />
          <Row label={"Author:"} value={poiData?.created_by} />
          <Row label={"Created:"} value={poiData?.created_at} />
          <Row label={"Updated:"} value={poiData?.updated_at} />
          <Row label={"Slug:"} value={poiData?.slug} />
          <Row label={"Desc:"} value={poiData?.description} />
        </div>
      </div>
    </WatchForOutsideClick>
  );
};
export default PoiDetail;

const styles = {
  container: {
    boxSizing: "contentBox",
    position: "absolute",
    zIndex: 3,
    background: "white",
    opacity: "0.9",
    width: "20rem",
    height: "100%",
    padding: "1rem",
    overflowY: "scroll"
  },
  hr: {
    width: "100%",
    borderBottom: "1px solid black",
    margin: ".5rem 0 .5rem 0"
  },
  poiName: { fontWeight: 900, fontSize: "1.2rem" },
  header: {
    color: color.accent,
    fontSize: ".9rem",
    minWidth: "3rem",
    fontWeight: 900,
    marginRight: ".5rem"
  },
  row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    margin: ".5rem 0 .5rem 0"
  }
};
