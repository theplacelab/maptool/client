import React, { useRef, useEffect, useState } from "react";
import ReactDOM from "react-dom/client";
import maplibregl from "maplibre-gl";
import "maplibre-gl/dist/maplibre-gl.css";
import useDeepCompareEffect from "use-deep-compare-effect";
import {
  MARKER,
  createFeaturesFromPoi,
  CircleMarker,
  ImageMarker,
  PinMarker
} from "./Markers";
import { Spinner, Menu } from "src/components";
import { Title } from "./Title";
import { Info } from "./Info";
import { Error } from "src/pages";

import { isValidMapStyle } from "src/utils.js";
import { color, icon } from "src/styles";
import { useGetPoiQuery } from "src/services/map";

let timer;

const MapDisplay = ({
  id,
  embedded,
  config,
  style,
  onPoiDragEnd,
  onPoiClick,
  poiContextMenu,
  onMapClick,
  poi,
  draggingPoiId,
  mapContextMenu,
  hasHeaderNavigation = false
}) => {
  const styles = {
    mapWrapper: {
      height: "100vh",
      width: "100vw",
      padding: 0,
      margin: 0,
      ...style
    },
    mapContainer: { width: "100%", height: "100%" }
  };
  const [isLoading, setIsLoading] = useState(true);
  const [menuItems, setMenuItems] = useState(null);
  const [lng, setLng] = useState(0);
  const [rect, setRect] = useState({ top: 0, left: 0 });
  const [lat, setLat] = useState(0);
  const [zoom, setZoom] = useState(
    config?.zoomInitial ? config.zoomInitial : 0
  );
  const [mousePos, setMousePos] = useState({});
  const backgroundColor = useRef(config.backgroundColor);
  const zoomInitial = useRef(config.zoomInitial);
  const mapObject = useRef(null);
  const [mousePosCoordinates, setMousePosCoordinates] = useState({});
  const [currentMarker, setCurrentMarker] = useState(null);

  // Init
  const mapContainer = useRef(null);
  const mapWrapper = useRef(null);
  const invalidConfig = !isValidMapStyle(config.style);

  // Add scroll listener
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setRect(mapWrapper.current?.getBoundingClientRect());
    });
    setRect(mapWrapper.current?.getBoundingClientRect());
  }, [setRect, mapWrapper]);

  // Deep compare avoids reloading on shallow
  useDeepCompareEffect(() => {
    if (invalidConfig) return;

    const mapSetup = () => {
      if (mapObject.current) {
        try {
          mapObject.current.remove();
        } catch {}
      }
      const mapObj = new maplibregl.Map({
        container: mapContainer.current,
        style: config?.style,
        center: config?.centerInitial
          ? JSON.parse(config?.centerInitial)
          : { lat: 0, lng: 0 },
        zoom
      });

      // Setup map
      mapContainer.current.innerHtml = null;

      // Navigation control (+/- zoom buttons)
      if (config.hasNavigation)
        mapObj.addControl(new maplibregl.NavigationControl(), "bottom-right");

      // Click handlers
      mapObj.on("click", (e) => {
        e.originalEvent.stopPropagation();
        e.originalEvent.preventDefault();
        if (typeof onMapClick === "function") onMapClick(config);
      });

      mapObj.on("contextmenu", (e) => {
        e.originalEvent.stopPropagation();
        e.originalEvent.preventDefault();
        setMenuItems(mapContextMenu);
        //onMapContextClick({ config, contextMenu: mapContextMenu });
      });

      mapObj.on("move", () => {
        setLng(mapObj.getCenter().lng.toFixed(4));
        setLat(mapObj.getCenter().lat.toFixed(4));
        setZoom(mapObj.getZoom().toFixed(2));
      });

      mapObj.on("mousemove", (e) => {
        setMousePos({
          x: e.originalEvent.clientX,
          y: e.originalEvent.clientY
        });
        setMousePosCoordinates({
          lng: e.lngLat.lng,
          lat: e.lngLat.lat
        });
      });

      mapObj.on("load", () => {
        if (mapObject.current && config.zoomInitial !== zoomInitial.current) {
          zoomInitial.current = config.zoomInitial;
          try {
            mapObject.current.setZoom(config.zoomInitial);
          } catch (e) {
            console.log(e);
          }
        }
        if (mapObject.current && config.centerInitial) {
          mapObject.current.setCenter(JSON.parse(config.centerInitial));
        }
        setIsLoading(false);
      });
      mapObject.current = mapObj;
    };

    if (config.backgroundColor !== backgroundColor.current) {
      backgroundColor.current = config.backgroundColor;
      clearTimeout(timer);
      setIsLoading(true);
      timer = setTimeout((msg) => {
        mapSetup();
      }, 1000);
      return;
    }

    mapSetup();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [config, invalidConfig]);

  useEffect(() => {
    // Render POI Markers
    if (!mapObject.current) return;
    if (!config?.style?.sources) return;
    document.querySelectorAll(".marker").forEach((marker) => {
      marker.remove();
    });
    createFeaturesFromPoi(poi)?.forEach((feature, idx) => {
      const ref = React.createRef();
      const isSelected = draggingPoiId === feature.id;
      ref.current = document.createElement("div");
      ref.current.className = "marker";
      const root = ReactDOM.createRoot(ref.current);
      const onMarkerContextClick = () => {
        setCurrentMarker(poi[idx]);
        setMenuItems(poiContextMenu);
      };
      switch (feature.markerType) {
        case MARKER.CIRCLE:
          root.render(
            <CircleMarker
              isSelected={isSelected}
              onClick={onPoiClick}
              onContextClick={onMarkerContextClick}
              feature={feature}
            />
          );
          break;
        case MARKER.IMAGE:
          root.render(
            <ImageMarker
              isSelected={isSelected}
              onClick={onPoiClick}
              onContextClick={onMarkerContextClick}
              feature={feature}
            />
          );
          break;
        case MARKER.PIN:
        default:
          root.render(
            <PinMarker
              isSelected={isSelected}
              onClick={onPoiClick}
              onContextClick={onMarkerContextClick}
              feature={feature}
            />
          );
          break;
      }
      const marker = new maplibregl.Marker(ref.current)
        .setLngLat(feature.geometry.coordinates)
        .setDraggable(draggingPoiId === feature.id)
        .addTo(mapObject.current);
      marker.on("dragend", () => onPoiDragEnd(marker, poi[idx]));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [poi, draggingPoiId, mapObject.current]);

  return (
    <React.Fragment>
      <div
        ref={mapWrapper}
        style={{
          ...styles.mapWrapper,
          overflow: "hidden",
          background: config.backgroundColor
        }}
      >
        {menuItems && (
          <Menu
            marker={currentMarker}
            map={mapObject.current}
            top={mousePos.y - rect.top - 10}
            left={mousePos.x - rect.left + 200}
            mouseAtCoordinates={mousePosCoordinates}
            onClose={() => {
              setMenuItems(null);
              setCurrentMarker(null);
            }}
            items={menuItems}
          />
        )}
        {!invalidConfig && isLoading && <Spinner />}
        {invalidConfig && (
          <Error
            embedded={embedded}
            backgroundColor={color.accent2}
            message={"Invalid Map Style"}
            icon={icon.INVALID}
          />
        )}
        <div
          style={{
            position: "relative",
            top: hasHeaderNavigation ? "3rem" : "0rem"
          }}
        >
          {config.hasTitle && <Title title={config.title} />}
          {config.hasInfo && <Info lat={lat} lng={lng} zoom={zoom} />}
        </div>
        <div ref={mapContainer} style={styles.mapContainer} />
      </div>
    </React.Fragment>
  );
};

export default MapDisplay;
