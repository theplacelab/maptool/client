import React from "react";
import { color } from "src/styles.jsx";

export const MARKER = {
  CIRCLE: "CIRCLE",
  IMAGE: "IMAGE",
  PIN: "PIN"
};

export const createFeaturesFromPoi = (poi) => {
  const features = [];
  if (poi && poi.length > 0)
    poi?.forEach((item) => {
      if (item.coordinates === null) {
        console.warn(`Skipping Render: POI ${item.id} has null coordinates`);
        return;
      }
      features.push({
        id: item.id,
        markerType: item.type,
        options: item.options,
        type: "Feature",
        properties: {
          title: item.title,
          description: item.description
        },
        geometry: {
          coordinates: [item.coordinates.lng, item.coordinates.lat],
          type: "Point"
        }
      });
    });
  return features;
};

//https://stackoverflow.com/questions/48048957/react-long-press-event
export const Base = ({
  onClick,
  onContextClick,
  children,
  feature,
  isSelected,
  size = 2
}) => {
  const content = (
    <div
      style={{ margin: "auto" }}
      onClick={(e) => {
        e.stopPropagation();
        e.preventDefault();
        onClick(feature);
      }}
      onContextMenu={(e) => {
        e.stopPropagation();
        e.preventDefault();
        onContextClick(feature);
      }}
    >
      {children}
    </div>
  );

  if (isSelected) {
    return (
      <div
        style={{
          height: `${size * 2}rem`,
          width: `${size * 2}rem`,
          display: "flex",
          alignItems: "center",
          borderRadius: `${size * 2}rem`,
          background: `radial-gradient(${color.poiMoveHighlight} 5%, transparent 70%)`,
          padding: "2rem",
          zIndex: 20
        }}
      >
        {content}
      </div>
    );
  } else {
    return content;
  }
};

export const CircleMarker = ({
  feature,
  onClick,
  onContextClick,
  isSelected = false
}) => (
  <Base
    onClick={onClick}
    onContextClick={onContextClick}
    feature={feature}
    isSelected={isSelected}
  >
    <div
      style={{
        background: feature.options.fill,
        width: feature.options.size,
        height: feature.options.size,
        borderRadius: feature.options.size,
        border: feature.options.border ? feature.options.border : "0px",
        boxShadow: "4px 4px 6px 0px #888888"
      }}
    />
  </Base>
);

export const ImageMarker = ({
  feature,
  onClick,
  onContextClick,
  isSelected = false,
  style
}) => (
  <Base
    onClick={onClick}
    onContextClick={onContextClick}
    feature={feature}
    isSelected={isSelected}
    size={feature.options.size}
  >
    <img
      alt={feature.description}
      src={feature.options.image}
      style={{
        margin: "auto",
        width: feature.options.size ? feature.options.size : "30px",
        borderRadius: feature.options.size ? feature.options.size : "30px",
        border: feature.options.border ? feature.options.border : "0px",
        boxShadow: "4px 4px 6px 0px #888888",
        ...style
      }}
    />
  </Base>
);

export const PinMarker = ({
  feature,
  onClick,
  onContextClick,
  isSelected = false
}) => (
  <ImageMarker
    isSelected={isSelected}
    onClick={onClick}
    onContextClick={onContextClick}
    feature={{
      ...feature,
      options: { ...feature.options, image: "/pin.png" }
    }}
    style={{ boxShadow: "none" }}
  />
);
