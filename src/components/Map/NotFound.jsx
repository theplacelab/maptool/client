import { color, icon } from "src/styles.jsx";
import { IconContext } from "react-icons";

const NotFound = () => {
  return (
    <div
      style={{
        backgroundColor: color.backgroundTint3,
        display: "flex",
        alignItems: "center",
        height: "100vh",
        width: "100vw"
      }}
    >
      <div style={{ margin: "auto", textAlign: "center", color: color.accent }}>
        <IconContext.Provider
          value={{
            color: color.accent,
            size: "5rem"
          }}
        >
          {icon.MAP_NOT_FOUND}
          <br />
          Map Not Found
        </IconContext.Provider>
      </div>
    </div>
  );
};

export default NotFound;
