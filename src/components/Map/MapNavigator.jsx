import { Button } from "src/components";
import { useSelector } from "react-redux";
import { default as useStyles, color, icon } from "src/styles.jsx";
import { useGetGlobalSettingsQuery } from "src/services/map";

import useMousetrap from "src/hooks/useMousetrap";

const MapNavigator = ({
  onMapSelect,
  currentMapId,
  availableMapList,
  embedded
}) => {
  const compactView = useSelector((state) => state.settings.compact);
  const currentIndex = availableMapList.indexOf(
    availableMapList.find((item) => item.id === currentMapId)
  );

  const { data: globalSettings } = useGetGlobalSettingsQuery();
  const globalMapSettings = globalSettings?.map;

  const nextMap = availableMapList[currentIndex + 1];
  const previousMap = availableMapList[currentIndex - 1];
  const classes = useStyles();

  const onPrevious = () => {
    if (previousMap && globalMapSettings?.hasPagination) {
      onMapSelect({ target: { value: previousMap.id } });
    }
  };
  const onNext = () => {
    if (nextMap && globalMapSettings?.hasPagination) {
      onMapSelect({ target: { value: nextMap.id } });
    }
  };
  useMousetrap("right", onNext, "keyup");
  useMousetrap("left", onPrevious, "keyup");

  if (!availableMapList) return null;

  return (
    <div
      style={{
        ...styles.container,
        width: embedded
          ? compactView
            ? "100vw"
            : "calc(100vw - 17rem)"
          : "100vw"
      }}
    >
      {globalMapSettings?.hasPagination && (
        <div style={{ marginLeft: "1rem" }}>
          <Button
            style={{ height: "2.5rem", width: "2.5rem" }}
            label={<div style={{ margin: "-0.2rem" }}>{icon.PREVIOUS}</div>}
            disabled={!previousMap}
            onClick={onPrevious}
            className={classes.smallButton}
          />
        </div>
      )}
      {!globalMapSettings?.hasMapSelector &&
        globalMapSettings?.hasPagination && (
          <div
            style={{
              margin: "1rem",
              flexGrow: "1",
              textAlign: globalMapSettings?.hasPagination ? "center" : "left"
            }}
          />
        )}
      {globalMapSettings?.hasMapSelector && (
        <div
          style={{
            margin: "1rem",
            flexGrow: "1",
            textAlign: globalMapSettings?.hasPagination ? "center" : "left"
          }}
        >
          <select
            style={{
              background: color.accent,
              border: 0,
              fontSize: "1.5rem",
              borderRadius: "0.3rem",
              height: "2.5rem"
            }}
            key={currentMapId}
            defaultValue={currentMapId}
            onChange={onMapSelect}
          >
            {availableMapList.map((item, idx) => (
              <option value={item.id} key={idx}>
                {item.title}
              </option>
            ))}
          </select>
        </div>
      )}
      {globalMapSettings?.hasPagination && (
        <div style={{ marginRight: "1rem" }}>
          <Button
            style={{ height: "2.5rem", width: "2.5rem" }}
            label={<div style={{ margin: "-0.2rem" }}>{icon.NEXT}</div>}
            disabled={!nextMap}
            onClick={onNext}
            className={classes.smallButton}
          />
        </div>
      )}
    </div>
  );
};
export default MapNavigator;

const styles = {
  container: {
    height: "4rem",
    position: "absolute",
    zIndex: 2,
    display: "flex",
    alignItems: "center"
  }
};
