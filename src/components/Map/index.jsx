import React, { useState, useEffect } from "react";
import MapDisplay from "./MapDisplay.jsx";
import { useGetMapsQuery } from "src/services/map";
import { useNavigate, useParams } from "react-router-dom";
import MapNavigator from "./MapNavigator.jsx";
import MapStyleEditor from "./MapStyleEditor.jsx"; //Just for export
import { AlertBar } from "src/components";
import { setAlert } from "src/redux/slice/alert";
import { useDispatch } from "react-redux";
import PoiDetail from "./PoiDetail.jsx";
import { MARKER } from "./Markers";
import NotFound from "./NotFound.jsx";
import { color } from "src/styles.jsx";
import { copyTextToClipboard } from "src/utils.js";
import { useGetPoiQuery, useGetGlobalSettingsQuery } from "src/services/map";

const Map = ({ embedded = false }) => {
  const dispatch = useDispatch();
  const params = useParams();
  const navigate = useNavigate();
  const { data: availableMapList } = useGetMapsQuery();
  const [displayMapMissing, setDisplayMapMissing] = useState(false);
  const [currentMapId, setCurrentMapId] = useState(null);
  const [draggingPoiId, setDraggingPoiId] = useState(null);
  const [poiData, setPoiData] = useState(null);

  const [editingPoiId, setEditingPoiId] = useState(null);
  const { data: globalSettings } = useGetGlobalSettingsQuery();
  const hasHeaderNavigation =
    globalSettings?.map.hasPagination || globalSettings?.map.hasMapSelector;
  const { data: existingPoiData } = useGetPoiQuery(currentMapId, {
    skip: !currentMapId
  });

  const _makeInitialCenter = (map, coordinates) => {
    map.flyTo({ center: coordinates });
    dispatch(
      setAlert({
        text: `Initial center set to ${coordinates.lat}, ${coordinates.lng}`,
        color: color.accent2,
        expiresAfter: 2000
      })
    );
  };

  // Default to first map in the list
  useEffect(() => {
    if (currentMapId === null && availableMapList?.length > 0) {
      setCurrentMapId(availableMapList[0].id);
    }
  }, [availableMapList, currentMapId]);

  // Display map-missing after delay to allow for loading
  useEffect(() => {
    setTimeout(() => setDisplayMapMissing(true), 1000);
  }, []);

  // Preload existing POI data
  useEffect(() => {
    if (!existingPoiData) return;
    setPoiData(structuredClone(existingPoiData));
  }, [existingPoiData]);

  // If map slug is provided, try and find it
  useEffect(() => {
    if (params.map_slug && availableMapList) {
      const selectedMap = availableMapList?.find(
        (item) => item.slug === params.map_slug
      );
      if (!selectedMap) {
        console.error(`MAP: "${params.map_slug}" not found! Loading default.`);
        setCurrentMapId(null);
      } else {
        // Load poi
        const mapId =
          typeof selectedMap !== "undefined" ? selectedMap.id : null;
        setCurrentMapId(mapId);
      }
    }
  }, [availableMapList, params.map_slug]);

  // If poi slug is provided, navigate there
  useEffect(() => {
    if (params.poi_slug)
      console.warn(`Unimplemented: Navigate to POI: ${params.poi_slug}`);
  }, [params.poi_slug]);

  // Parse the map configuration
  const currentMap =
    currentMapId !== null
      ? availableMapList.find((item) => item.id === currentMapId)
      : null;
  if (!currentMap) return displayMapMissing ? <NotFound /> : null;

  return (
    <div>
      {editingPoiId && (
        <PoiDetail
          mapId={currentMapId}
          id={editingPoiId}
          onClose={() => setEditingPoiId(null)}
        />
      )}
      {!embedded && <AlertBar />}
      <MapNavigator
        embedded={embedded}
        currentMapId={currentMapId}
        availableMapList={availableMapList}
        onMapSelect={(e) => {
          const id = parseInt(e.target.value, 10);
          const selectedMap = availableMapList.find((item) => item.id === id);
          if (embedded) {
            navigate(`/maptool/preview/${selectedMap.slug}`);
          } else {
            navigate(`/${selectedMap.slug}`);
          }
        }}
      />
      <MapDisplay
        id={currentMapId}
        embedded={embedded}
        hasHeaderNavigation={hasHeaderNavigation}
        config={{
          ...currentMap,
          style: currentMap.style_json,
          ...currentMap.settings_json
        }}
        draggingPoiId={draggingPoiId}
        poi={poiData}
        onPoiDragEnd={(marker, poi) => {
          const newCoordinates = {
            lng: marker.getLngLat().lng,
            lat: marker.getLngLat().lat
          };
          const updatedPoiData = [...poiData];
          updatedPoiData[updatedPoiData.indexOf(poi)].coordinates =
            newCoordinates;
          setPoiData(updatedPoiData);
          setDraggingPoiId(null);
        }}
        onPoiClick={(poi) => setEditingPoiId(poi.id)}
        mapContextMenu={[
          {
            label: "Add POI",
            action: ({ coordinates }) => {
              setPoiData((existingPoiData) => {
                const updatedPoiData = [
                  ...(existingPoiData ? existingPoiData : [])
                ];
                const id = Math.max(...updatedPoiData.map((poi) => poi.id)) + 1;
                updatedPoiData.push({
                  id,
                  coordinates,
                  description: "New",
                  title: "New",
                  type: MARKER.PIN,
                  options: { size: "2rem" }
                });
                return updatedPoiData;
              });
            }
          },
          {
            label: "Copy Coordinates",
            action: ({ coordinates }) => {
              copyTextToClipboard(JSON.stringify(coordinates));
              dispatch(
                setAlert({
                  text: "Coordinates copied to clipboard",
                  color: color.accent2,
                  expiresAfter: 2000
                })
              );
            }
          }
        ]}
        poiContextMenu={[
          {
            label: "Reposition",
            action: ({ marker }) => setDraggingPoiId(marker.id)
          },
          {
            label: "Edit",
            action: ({ map, marker }) => {
              map.flyTo({
                center: marker.coordinates,
                duration: 1200,
                zoom: 18,
                essential: true
              });
              setEditingPoiId(marker.id);
            }
          },
          {
            label: "Delete",
            action: ({ marker }) => {
              const updatedPoiData = [...poiData];
              updatedPoiData.splice(
                updatedPoiData.indexOf(
                  updatedPoiData.find((poi) => poi.id === marker.id)
                ),
                1
              );
              setPoiData(updatedPoiData);
            }
          },
          {
            label: "Copy Link",
            action: ({ marker }) => {
              copyTextToClipboard(`http://example.com/poi/${marker.id}`);
              dispatch(
                setAlert({
                  text: "Link copied to clipboard",
                  color: color.accent2,
                  expiresAfter: 2000
                })
              );
            }
          },
          {
            label: "Copy Coordinates",
            action: ({ marker }) => {
              copyTextToClipboard(JSON.stringify(marker.coordinates));
              dispatch(
                setAlert({
                  text: "Coordinates copied to clipboard",
                  color: color.accent2,
                  expiresAfter: 2000
                })
              );
            }
          },
          {
            label: "Make Initial Center",
            action: ({ map, marker }) => {
              _makeInitialCenter(map, marker.coordinates);
            }
          }
        ]}
      />{" "}
    </div>
  );
};

export default Map;
export { MapStyleEditor };
