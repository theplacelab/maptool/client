import React from "react";
import { color } from "src/styles.jsx";

const IndicatorLight = ({ label, up }) => {
  return (
    <div
      title={`${label}: ${up ? "UP" : "DOWN"}`}
      style={{
        backgroundColor: up ? color.accent : color.offline,
        borderRadius: "1rem",
        minHeight: "1rem",
        minWidth: "1rem",
        maxHeight: "1rem",
        maxWidth: "1rem",
        border: `1px solid ${color.accent2}`,
        cursor: "pointer",
        display: "inline-block",
        margin: ".2rem",
        backgroundImage: up
          ? ""
          : 'url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc1JyBoZWlnaHQ9JzUnPgogIDxyZWN0IHdpZHRoPSc1JyBoZWlnaHQ9JzUnIGZpbGw9J3doaXRlJy8+CiAgPHBhdGggZD0nTTAgNUw1IDBaTTYgNEw0IDZaTS0xIDFMMSAtMVonIHN0cm9rZT0nIzg4OCcgc3Ryb2tlLXdpZHRoPScxJy8+Cjwvc3ZnPg==")',
        backgroundRepeat: "repeat"
      }}
    />
  );
};
export default IndicatorLight;
