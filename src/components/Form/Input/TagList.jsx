import React, { useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import WatchForOutsideClick from "src/components/WatchForOutsideClick";

const TagList = ({
  disabled = false,
  autoComplete,
  id,
  label,
  type,
  placeholder,
  onChange = () => console.warn("<Text/>: onChange not defined"),
  onBlur,
  value,
  style
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const classes = useStyles();

  return (
    <div style={{ position: "relative" }}>
      <div className={classes.inputTextLabel}>{label}</div>
      {((isEditing || !value) && (
        <WatchForOutsideClick onOutsideClick={() => setIsEditing(false)}>
          <input
            disabled={disabled}
            style={style}
            value={value ? value : ""}
            autoComplete={autoComplete}
            id={id}
            onBlur={(e) => {
              if (typeof onBlur === "function") onBlur(e);
            }}
            onChange={(e) => {
              if (typeof onChange === "function") onChange(e);
            }}
            className={classes.inputText}
            label={label}
            type={type}
            placeholder={placeholder}
          />
        </WatchForOutsideClick>
      )) || (
        <div
          className={classes.tagList}
          onClick={() => {
            if (!disabled) setIsEditing(true);
          }}
        >
          {value?.split(",").map((tag, idx) => (
            <div key={idx} className={classes.tag}>
              {tag}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
export default TagList;
