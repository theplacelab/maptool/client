import React from "react";
import { default as useStyles } from "src/styles.jsx";

const Color = ({
  disabled = false,
  autoComplete,
  id,
  label,
  onChange = () => console.warn("<Color/>: onChange not defined"),
  value = "#FFFFFF"
}) => {
  const classes = useStyles();
  return (
    <div style={{ position: "relative" }}>
      <div style={{ top: "-1.1rem" }} className={classes.inputTextLabel}>
        {label}
      </div>
      <input
        disabled={disabled}
        style={{ height: "3rem", width: "5rem" }}
        value={value ? value : ""}
        autoComplete={autoComplete}
        id={id}
        onChange={(e) => onChange(e)}
        label={label}
        type={"color"}
      />
    </div>
  );
};
export default Color;
