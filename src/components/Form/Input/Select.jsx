import React from "react";
import { default as useStyles } from "src/styles.jsx";

const Select = ({
  disabled = false,
  options,
  id,
  label,
  overlay,
  onChange = () => console.warn("<Select/>: onChange not defined"),
  value,
  style
}) => {
  const classes = useStyles();

  return (
    <div style={{ position: "relative" }}>
      <div className={classes.inputTextLabel}>{label}</div>
      <div style={{ position: "absolute", right: "1rem", top: "1.5rem" }}>
        {overlay}
      </div>

      <select
        value={value ? value : ""}
        className={classes.inputSelect}
        onChange={(e) => {
          if (typeof onChange === "function") onChange(e);
        }}
        disabled={disabled}
        style={style}
        label={label}
        id={id}
        name={id}
      >
        {options?.map((option, idx) => {
          return (
            <option key={`${id}_${idx}`} style={style} value={option.id}>
              {option.display_name}
            </option>
          );
        })}
      </select>
    </div>
  );
};
export default Select;
