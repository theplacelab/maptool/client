import React, { useState } from "react";
import WatchForOutsideClick from "src/components/WatchForOutsideClick";
import { default as useStyles } from "src/styles.jsx";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";

const TextArea = ({
  markdown = false,
  disabled = false,
  autoComplete,
  id,
  label,
  type,
  placeholder,
  overlay,
  onChange = () => console.warn("<TextArea/>: onChange not defined"),
  value,
  style
}) => {
  const classes = useStyles();
  const [displayMarkdownEditor, setDisplayMarkdownEditor] = useState(!markdown);

  if (markdown && !displayMarkdownEditor)
    return (
      <div style={{ position: "relative", ...style }}>
        <div onClick={() => setDisplayMarkdownEditor(!displayMarkdownEditor)}>
          <div className={classes.inputTextLabel}>{label}</div>
          <div
            className={classes.inputTextArea}
            style={{
              backgroundColor: disabled ? "#dddddd40" : "transparent"
            }}
          >
            <ReactMarkdown rehypePlugins={[rehypeRaw]}>{value}</ReactMarkdown>
          </div>
        </div>
      </div>
    );

  return (
    <WatchForOutsideClick
      onOutsideClick={() => setDisplayMarkdownEditor(false)}
    >
      <div style={{ position: "relative", ...style }}>
        <div className={classes.inputTextLabel}>{label}</div>
        <div style={{ position: "absolute", right: "1rem", top: "1.5rem" }}>
          {overlay}
        </div>
        <textarea
          disabled={disabled}
          style={{ height: "100%", width: "100%" }}
          value={value ? value : ""}
          autoComplete={autoComplete}
          id={id}
          onChange={onChange}
          className={classes.inputTextArea}
          label={label}
          type={type}
          placeholder={placeholder}
        />
      </div>
    </WatchForOutsideClick>
  );
};
export default TextArea;
