import React from "react";
import { default as useStyles } from "src/styles.jsx";

const Checkbox = ({
  disabled = false,
  autoComplete,
  id,
  label,
  placeholder,
  onChange = () => console.warn("<Checkbox/>: onChange not defined"),
  value
}) => {
  const classes = useStyles();
  return (
    <div
      style={{
        position: "relative",
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
      }}
    >
      <div>
        <input
          disabled={disabled}
          style={{ height: "2rem", width: "2rem" }}
          checked={value ? value : false}
          autoComplete={autoComplete}
          id={id}
          onChange={(e) =>
            onChange({ target: { id, value: e.currentTarget.checked } })
          }
          label={label}
          type="checkbox"
          placeholder={placeholder}
        />
      </div>
      <div className={classes.inputCheckboxLabel}>{label}</div>
    </div>
  );
};
export default Checkbox;
