import { icon } from "src/styles.jsx";
import React, { useEffect, useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import validator, { isNumeric } from "validator";

const Coordinates = ({
  disabled = false,
  autoComplete,
  id,
  label,
  type,
  placeholder,
  overlay,
  onChange = () => console.warn("<Coordinates/>: onChange not defined"),
  onBlur,
  value,
  style
}) => {
  const classes = useStyles();
  const [lat, setLat] = useState(
    value ? (isNumeric(value.lat) ? value.lat : 0) : 0
  );
  const [lng, setLng] = useState(
    value ? (isNumeric(value.lng) ? value.lng : 0) : 0
  );

  const handleOnChange = (e) => {
    let newLat = lat;
    let newLng = lng;

    if (e.target.id === "lat") {
      newLat = e.target.value;
      setLat(newLat);
    }
    if (e.target.id === "lng") {
      newLng = e.target.value;
      setLng(newLng);
    }

    /*
    if (typeof newLat === "string" && newLat.includes(",")) {
      const coordinates = newLat.split(",");
      setLat(coordinates[0]);
      setLng(coordinates[1]);
    }
    */

    if (typeof onChange === "function")
      onChange({ target: { id, value: { lat: newLat, lng: newLng } } });
  };

  return (
    <div style={{ position: "relative" }}>
      <div className={classes.inputTextLabel}>
        {validator.isLatLong(`${lat},${lng}`)
          ? icon.COLOR.VALID
          : icon.COLOR.INVALID}{" "}
        {label}
      </div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <input
          id={"lat"}
          disabled={disabled}
          style={style}
          value={lat}
          autoComplete={autoComplete}
          onBlur={(e) => {
            if (typeof onBlur === "function") onBlur(e);
          }}
          onChange={handleOnChange}
          className={classes.inputText}
          label={label}
          type={type}
          placeholder={placeholder}
        />
        <input
          id={"lng"}
          disabled={disabled}
          style={style}
          value={lng}
          autoComplete={autoComplete}
          onBlur={(e) => {
            if (typeof onBlur === "function") onBlur(e);
          }}
          onChange={handleOnChange}
          className={classes.inputText}
          label={label}
          type={type}
          placeholder={placeholder}
        />
      </div>
    </div>
  );
};
export default Coordinates;
