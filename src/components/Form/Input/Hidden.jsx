import React from "react";

const Hidden = ({ id, label, value }) => {
  return (
    <input value={value ? value : ""} id={id} label={label} type={"hidden"} />
  );
};
export default Hidden;
