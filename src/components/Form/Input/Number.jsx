import React from "react";
import { default as useStyles } from "src/styles.jsx";

const Text = ({
  disabled = false,
  autoComplete,
  id,
  label,
  placeholder,
  overlay,
  onChange = () => console.warn("<Number/>: onChange not defined"),
  value
}) => {
  const classes = useStyles();
  return (
    <div style={{ position: "relative" }}>
      <div className={classes.inputTextLabel}>{label}</div>
      <div style={{ position: "absolute", right: "1rem", top: "1.5rem" }}>
        {overlay}
      </div>
      <input
        disabled={disabled}
        value={value ? value : ""}
        autoComplete={autoComplete}
        id={id}
        onChange={(e) => {
          onChange(e);
        }}
        className={classes.inputText}
        label={label}
        type={"number"}
        placeholder={placeholder}
      />
    </div>
  );
};
export default Text;
