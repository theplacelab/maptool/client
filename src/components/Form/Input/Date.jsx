import React from "react";
import { default as useStyles } from "src/styles.jsx";

const Date = ({
  disabled = false,
  autoComplete,
  id,
  label,
  placeholder,
  overlay,
  onChange = () => console.warn("<Date/>: onChange not defined"),
  onBlur,
  value,
  style
}) => {
  const classes = useStyles();
  return (
    <div style={{ position: "relative" }}>
      <div className={classes.inputTextLabel}>{label}</div>
      <div style={{ position: "absolute", right: "1rem", top: "1.5rem" }}>
        {overlay}
      </div>
      <input
        type="date"
        disabled={disabled}
        style={style}
        value={value ? value : ""}
        autoComplete={autoComplete}
        id={id}
        onBlur={(e) => {
          if (typeof onBlur === "function") onBlur(e);
        }}
        onChange={(e) => {
          if (typeof onChange === "function") onChange(e);
        }}
        className={classes.inputText}
        label={label}
        placeholder={placeholder}
      />
    </div>
  );
};
export default Date;
