import React from "react";
import { default as useStyles } from "src/styles.jsx";

const Text = ({
  disabled = false,
  autoComplete,
  id,
  label,
  type,
  placeholder,
  overlay,
  onChange = () => console.warn("<Text/>: onChange not defined"),
  onBlur,
  value,
  style
}) => {
  const classes = useStyles();
  return (
    <div style={{ position: "relative" }}>
      <div className={classes.inputTextLabel}>{label}</div>
      <div style={{ position: "absolute", right: "1rem", top: "1.5rem" }}>
        {overlay}
      </div>
      <input
        disabled={disabled}
        style={style}
        value={value ? value : ""}
        autoComplete={autoComplete}
        id={id}
        onBlur={(e) => {
          if (typeof onBlur === "function") onBlur(e);
        }}
        onChange={(e) => {
          if (typeof onChange === "function") onChange(e);
        }}
        className={classes.inputText}
        label={label}
        type={type}
        placeholder={placeholder}
      />
    </div>
  );
};
export default Text;
