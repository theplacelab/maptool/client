import React, { useState } from "react";
import { icon } from "src/styles.jsx";

import Text from "./Input/Text";
import TextArea from "./Input/TextArea";
import Number from "./Input/Number";
import Hidden from "./Input/Hidden";
import Select from "./Input/Select";
import Date from "./Input/Date";
import TagList from "./Input/TagList";
import Asset from "./Input/Asset";
import Checkbox from "./Input/Checkbox";
import Coordinates from "./Input/Coordinates";
import Color from "./Input/Color";

const FIELD_TYPE = {
  TEXT: "text",
  TEXTAREA: "textarea",
  PASSWORD: "password",
  COLOR: "color",
  CHECKBOX: "checkbox",
  NUMBER: "number",
  HIDDEN: "hidden",
  SELECT: "select",
  COORDINATES: "coordinates",
  ASSET: "asset",
  TAGLIST: "taglist",
  DATE: "date"
};

const FormElement = (props) => {
  const { type } = props;
  switch (type) {
    case FIELD_TYPE.COLOR:
      return <Color {...props} />;
    case FIELD_TYPE.PASSWORD:
      return <Text {...props} />;
    case FIELD_TYPE.TEXT:
      return <Text {...props} />;
    case FIELD_TYPE.HIDDEN:
      return <Hidden {...props} />;
    case FIELD_TYPE.TEXTAREA:
      return <TextArea {...props} />;
    case FIELD_TYPE.CHECKBOX:
      return <Checkbox {...props} />;
    case FIELD_TYPE.NUMBER:
      return <Number {...props} />;
    case FIELD_TYPE.SELECT:
      return <Select {...props} />;
    case FIELD_TYPE.COORDINATES:
      return <Coordinates {...props} />;
    case FIELD_TYPE.DATE:
      return <Date {...props} />;
    case FIELD_TYPE.ASSET:
      return <Asset {...props} />;
    case FIELD_TYPE.TAGLIST:
      return <TagList {...props} />;
    default:
      const msg = `FORM INPUT: Unknown type: ${type}`;
      console.warn(msg);
      console.log(props);
      return (
        <div style={{ color: "red" }}>
          &lt;FORMELEMENT /&gt;: "{type}" field not defined
        </div>
      );
  }
};

const Input = (props) => {
  const { type } = props;
  const [isPublished, setIsPublished] = useState(
    typeof props.published === "undefined" ? true : props.published
  );
  const [isMarkdown, setIsMarkdown] = useState(props.markdown);
  const [isLocked, setIsLocked] = useState(props.locked);
  const {
    disabled = false,
    editDefaults = false,
    onChange = () => console.warn("OnChange not defined")
  } = props;

  const el = (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
      }}
    >
      {editDefaults && (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center"
          }}
        >
          <div
            onClick={() => {
              onChange({
                target: { id: `${props.id}_locked`, value: !isLocked }
              });
              setIsLocked(!isLocked);
            }}
          >
            {isLocked ? icon.FIELD_STATUS.LOCKED : icon.FIELD_STATUS.UNLOCKED}
          </div>
          <div
            onClick={() => {
              onChange({
                target: { id: `${props.id}_published`, value: !isPublished }
              });
              setIsPublished(!isPublished);
            }}
          >
            {isPublished
              ? icon.FIELD_STATUS.PUBLISHED
              : icon.FIELD_STATUS.UNPUBLISHED}
          </div>
          <div
            onClick={() => {
              onChange({
                target: { id: `${props.id}_markdown`, value: !isMarkdown }
              });
              setIsMarkdown(!isMarkdown);
            }}
          >
            {props.type === FIELD_TYPE.TEXTAREA
              ? isMarkdown
                ? icon.FIELD_STATUS.MARKDOWN
                : icon.FIELD_STATUS.UNMARKDOWN
              : null}
          </div>
        </div>
      )}
      <div style={{ flexGrow: 1 }}>
        <FormElement
          {...props}
          markdown={isMarkdown}
          locked={isLocked}
          published={isPublished}
          disabled={disabled ? true : isLocked ? true : false}
          label={
            props.locked ? (
              editDefaults ? (
                props.label
              ) : (
                <div onClick={() => setIsLocked(!isLocked)}>
                  {isLocked
                    ? icon.FIELD_STATUS.LOCKED
                    : icon.FIELD_STATUS.UNLOCKED}{" "}
                  {props.label}
                </div>
              )
            ) : (
              props.label
            )
          }
        />
      </div>
    </div>
  );
  return (
    <div
      style={
        !isPublished && type !== FIELD_TYPE.HIDDEN
          ? {
              outline: "2px dotted #ffbebe",
              borderRadius: "0.3rem",
              margin: ".5rem 0",
              padding: ".6rem"
            }
          : {
              margin: ".5rem 0",
              padding: ".2rem",
              borderRadius: "0.3rem"
            }
      }
    >
      {el}
    </div>
  );
};

export { Input, FIELD_TYPE };
