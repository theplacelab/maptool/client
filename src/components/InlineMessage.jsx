import React from "react";
import { color } from "src/styles.jsx";

export default function Button({ message }) {
  return (
    <div
      style={{
        padding: "1rem",
        background: message.background ? message.background : color.inlineError,
        color: message.color ? message.color : color.inlineErrorText,
        fontWeight: 900
      }}
    >
      {message.text}
    </div>
  );
}
