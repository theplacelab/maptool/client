import React, { useState, useEffect } from "react";
import { default as useStyles } from "src/styles.jsx";
import { color, font, icon } from "src/styles.jsx";
import { IconContext } from "react-icons";
import { useSelector } from "react-redux";
import { Button } from "src/components";

const Table = ({ data, show, onClick, actions, onSaveSortOrder, cta }) => {
  const compactView = useSelector((state) => state.settings.compact);
  const classes = useStyles();
  const [direction, setDirection] = useState(false);
  const [searchString, setSearchString] = useState("");
  const [sortedData, setSortedData] = useState(null);
  const [saveEnabled, setSaveEnabled] = useState(false);
  const [sortBy, _setSortBy] = useState("");
  const setSortBy = (value) => {
    setSaveEnabled(true);
    _setSortBy(value);
  };
  const userSortable = typeof onSaveSortOrder === "function";

  useEffect(() => {
    if (data) {
      let sortedData = [...data];
      sortedData.sort((a, b) => {
        const keyA = direction ? a[sortBy] : b[sortBy];
        const keyB = direction ? b[sortBy] : a[sortBy];
        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        return 0;
      });
      setSortedData(sortedData);
    }
  }, [data, sortBy, direction]);

  const move = (arr, from, to) => {
    arr.splice(to, 0, arr.splice(from, 1)[0]);
  };

  const onMoveUp = (e, map) => {
    setSaveEnabled(true);
    const currentIdx = sortedData.indexOf(map);
    const reorderedData = [...sortedData];
    move(reorderedData, currentIdx, currentIdx - 1 >= 0 ? currentIdx - 1 : 0);
    setSortedData(reorderedData);
  };

  const onMoveDown = (e, map) => {
    setSaveEnabled(true);
    const currentIdx = sortedData.indexOf(map);
    const reorderedData = [...sortedData];
    move(
      reorderedData,
      currentIdx,
      currentIdx + 1 < sortedData.length ? currentIdx + 1 : sortedData.length
    );
    setSortedData(reorderedData);
  };

  const whitelist = show ? show : Object.keys(data[0]);
  const headerHtml = whitelist.map((name, idx) => {
    if (compactView && idx > 0) return null;
    return (
      <th
        key={`header_${idx}`}
        colSpan={userSortable && idx === 0 ? 2 : 1}
        style={{
          minWidth: "15rem",
          background: color.backgroundTint1,
          height: "2rem",
          border: `1px solid ${color.backgroundTint2}`,
          textAlign: "left",
          paddingLeft: ".5rem",
          fontFamily: font.display,
          fontSize: "1.3rem",
          color: color.accent
        }}
        onClick={() => {
          if (!userSortable) {
            setDirection(!direction);
            setSortBy(name);
          }
        }}
      >
        {name?.replaceAll("_", " ")}
        <span style={{ position: "relative", top: ".2rem" }}>
          {sortBy === name
            ? direction
              ? icon.CHEVRON_UP
              : icon.CHEVRON_DOWN
            : ""}
        </span>
      </th>
    );
  });

  let tableHtml = [];

  sortedData?.forEach((datum, idx) => {
    const row = [];
    let includeRow = searchString.length > 0 ? false : true;
    whitelist.forEach((header, idx2) => {
      if (compactView && idx > 0) return;
      row.push(
        <React.Fragment key={`${header}_${idx2}`}>
          {!compactView && userSortable && idx2 === 0 && (
            <td
              style={{
                border: `1px solid ${color.backgroundTint2}`,
                padding: ".5rem"
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <IconContext.Provider
                  value={{
                    color: color.accent,
                    size: "2rem"
                  }}
                >
                  <div style={{ flexGrow: 1 }} />
                  <div
                    onClick={(e) => {
                      e.stopPropagation();
                      e.preventDefault();
                      if (!(idx === 0)) onMoveUp(e, datum);
                    }}
                    style={{ opacity: idx === 0 ? 0.5 : 1 }}
                  >
                    {icon.ARROW_UP}
                  </div>
                  <div
                    onClick={(e) => {
                      e.stopPropagation();
                      e.preventDefault();
                      if (!(idx === sortedData.length - 1))
                        onMoveDown(e, datum);
                    }}
                    style={{ opacity: idx === sortedData.length - 1 ? 0.5 : 1 }}
                  >
                    {icon.ARROW_DOWN}
                  </div>
                  <div style={{ flexGrow: 1 }} />
                </IconContext.Provider>
              </div>
            </td>
          )}
          <td
            style={{
              border: `1px solid ${color.backgroundTint2}`,
              padding: ".5rem"
            }}
          >
            {datum[header]}
          </td>
        </React.Fragment>
      );
      if (searchString.length > 0 && !includeRow)
        includeRow = datum[header]?.toLowerCase().includes(searchString);
    });
    if (includeRow)
      tableHtml.push(
        <tr
          onClick={() => onClick(datum.uuid ? datum.uuid : datum.id)}
          key={`row_${idx}`}
        >
          {row}
        </tr>
      );
  });

  const noneFound =
    searchString.length > 0
      ? `No '${searchString.substring(0, 50)}' found`
      : "None Found";
  return (
    <React.Fragment>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <div>
            <input
              placeholder="Search..."
              className={classes.inputSearch}
              type="text"
              onChange={(e) =>
                setSearchString(e.target.value.toLowerCase().trim())
              }
            />
          </div>
          {userSortable && (
            <React.Fragment>
              <div style={{ flexGrow: 1 }} />
              <div style={{ display: "flex", flexDirection: "row" }}>
                <Button
                  disabled={!saveEnabled}
                  onClick={() => {
                    setSaveEnabled(false);
                    if (userSortable) onSaveSortOrder(sortedData);
                  }}
                  label={"Save Order"}
                  className={classes.smallButton}
                />
                {cta}
              </div>
            </React.Fragment>
          )}
        </div>
        <div style={{ flexGrow: 1 }}></div>
        <div>{actions}</div>
      </div>
      <table style={{ width: "100%", borderCollapse: "collapse" }}>
        <thead>
          <tr>{headerHtml}</tr>
        </thead>
        <tbody>
          {tableHtml.length > 0 ? (
            tableHtml
          ) : (
            <tr>
              <td
                colSpan={whitelist.length}
                style={{ color: color.accent, padding: ".5rem" }}
              >
                {noneFound}
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </React.Fragment>
  );
};
export default Table;
