import React from "react";
import { default as useStyles } from "src/styles.jsx";

export default function Button({
  label,
  onClick,
  style,
  type = "submit",
  disabled = false,
  className
}) {
  const classes = useStyles();
  return (
    <button
      onClick={disabled ? null : onClick}
      type={type}
      className={className ? className : classes.inputButton}
      style={{
        ...style,
        opacity: disabled ? 0.5 : 1
      }}
    >
      {label}
    </button>
  );
}
