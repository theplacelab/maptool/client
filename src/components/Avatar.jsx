import React from "react";
import { color } from "src/styles.jsx";

const Avatar = ({ user, size = 175 }) => {
  return (
    <React.Fragment>
      {(user?.avatar?.length > 0 && (
        <div>
          <img
            alt={user.name}
            src={user?.avatar}
            style={{
              minWidth: size,
              maxWidth: size,
              minHeight: size,
              maxHeight: size,
              borderRadius: "20rem",
              border: `.2rem solid ${color.backgroundTint2}`
            }}
          />
        </div>
      )) || (
        <div
          style={{
            minWidth: size,
            maxWidth: size,
            minHeight: size,
            maxHeight: size,
            background: color.accent,
            borderRadius: "20rem",
            border: `.2rem solid ${color.backgroundTint2}`,
            textAlign: "center",
            display: "flex",
            margin: "auto"
          }}
        >
          <div
            style={{
              margin: "auto",
              fontWeight: 900,
              fontSize: `calc(${size}px * .5)`,
              color: color.background
            }}
          >
            {user?.name ? user.name?.charAt(0) : "!"}
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
export default Avatar;
