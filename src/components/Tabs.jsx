import React, { Children, useState } from "react";
import { default as useStyles } from "src/styles.jsx";

const Tab = () => null;
const TabSet = ({ children, style, initialIndex = 0 }) => {
  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = useState(initialIndex);
  const arrayChildren = Children.toArray(children);
  return (
    <div style={style}>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <React.Fragment>
          {arrayChildren.map((tab, idx) => (
            <div
              className={
                idx === selectedIndex ? classes.tabSelected : classes.tab
              }
              key={`tab_${idx}`}
              onClick={() => setSelectedIndex(idx)}
            >
              {tab.props.title}
            </div>
          ))}
        </React.Fragment>
      </div>
      <div>{arrayChildren[selectedIndex].props.children}</div>
    </div>
  );
};
export { TabSet, Tab };
