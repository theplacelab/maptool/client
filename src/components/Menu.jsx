import React, { useEffect, useState, useRef } from "react";
import { color } from "src/styles.jsx";
import WatchForOutsideClick from "./WatchForOutsideClick";

const Menu = ({
  onClose,
  top,
  left,
  items,
  mouseAtCoordinates,
  map,
  marker
}) => {
  const [hover, setHover] = useState(0);
  const styles = {
    container: {
      width: "10rem",
      position: "absolute",
      zIndex: 5,
      background: color.backgroundTint2,
      borderRadius: ".3rem",
      top: `${top}px`,
      left: `${left}px`,
      boxShadow: "4px 4px 6px 0px #888888",
      padding: ".5rem",
      userSelect: "none"
    },
    hovered: {
      padding: ".2rem",
      background: color.accent,
      borderRadius: "0.2rem"
    },
    unHovered: { padding: ".2rem" }
  };

  // If menu is open and we get new top/left coordinate, just close
  const prevTopRef = useRef();
  const prevLeftRef = useRef();
  const menuVisible = useRef(false);
  useEffect(() => {
    if (
      menuVisible.current &&
      (left !== prevLeftRef.current || top !== prevTopRef.current)
    )
      onClose();
    menuVisible.current = true;
  }, [top, left, onClose]);

  // If we don't know where the mouse is yet don't draw menu
  if (isNaN(top) || isNaN(left)) return null;

  return (
    <WatchForOutsideClick onOutsideClick={onClose}>
      <div style={styles.container}>
        {items.map((item, idx) => (
          <div
            style={hover === idx ? styles.hovered : styles.unHovered}
            key={idx}
            onMouseOver={() => setHover(idx)}
            onMouseLeave={() => setHover()}
            onMouseDown={(e) => {
              e.preventDefault();
              e.stopPropagation();
              item.action({
                coordinates: mouseAtCoordinates,
                map,
                marker
              });
              onClose();
            }}
          >
            {item.label}
          </div>
        ))}
      </div>
    </WatchForOutsideClick>
  );
};
export default Menu;
