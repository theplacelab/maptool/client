import React from "react";
import { color } from "src/styles.jsx";
import { SpinnerInfinity } from "spinners-react";
export default function Spinner({ message = "" }) {
  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        height: "100%",
        width: "100%",
        background: color.foreground,
        opacity: 0.7,
        zIndex: 3,
        display: "flex",
        textAlign: "center",
        overflow: "hidden"
      }}
    >
      <div style={{ margin: "auto", color: color.foreground, fontWeight: 900 }}>
        <SpinnerInfinity
          size={76}
          thickness={159}
          speed={113}
          color={color.accent}
          secondaryColor="rgba(0, 0, 0, 0)"
        />
        <div style={{ marginTop: "1rem" }}>{message}</div>
      </div>
    </div>
  );
}
