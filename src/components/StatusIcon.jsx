import React from "react";
import { IconContext } from "react-icons";
import { color } from "src/styles.jsx";

const StatusIcon = ({ text, icon, active = true, onClick }) => {
  const styles = {
    container: {
      opacity: active ? 1 : 0.2,
      display: "flex",
      alignItems: "center",
      userSelect: "none"
    },
    icon: {
      marginRight: ".5rem",
      textAlign: "center"
    },
    iconContext: {
      color: color.accent,
      size: "2rem"
    }
  };
  return (
    <React.Fragment>
      <div style={styles.container} onClick={onClick}>
        <div style={styles.icon}>
          <IconContext.Provider value={styles.iconContext}>
            {icon}
          </IconContext.Provider>
        </div>
        <div style={{ marginRight: "1rem", whiteSpace: "nowrap" }}>{text}</div>
      </div>
    </React.Fragment>
  );
};

export default StatusIcon;
