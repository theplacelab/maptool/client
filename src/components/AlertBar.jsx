import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { clearAlert } from "src/redux/slice/alert";

export default function AlertBar() {
  const dispatch = useDispatch();
  const alert = useSelector((state) => state.alert);

  useEffect(() => {
    if (alert.expiresAfter) {
      const timer = setTimeout(
        () => dispatch(clearAlert()),
        alert.expiresAfter
      );
      return () => {
        clearTimeout(timer);
      };
    }
  }, [alert, dispatch]);

  if (alert.text?.length === 0) return null;
  return (
    <div
      onClick={() => dispatch(clearAlert())}
      style={{
        ...styles.bar,
        background: alert.color
      }}
    >
      {alert.text}
    </div>
  );
}
const styles = {
  bar: {
    color: "white",
    fontWeight: 900,
    position: "fixed",
    background: "red",
    zIndex: 10,
    width: "100vw",
    height: "3rem",
    lineHeight: "1rem",
    padding: "1rem",
    fontSize: ".8rem",
    boxShadow: "4px 4px 6px 0px #888888",
    userSelect: "none"
  }
};
