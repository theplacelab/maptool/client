import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./Collapsable.module.css";
import { IconContext } from "react-icons";
import { color, icon } from "src/styles.jsx";

class Collapsable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen
    };
  }

  toggleState = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  titleClick = () => {
    if (this.props.clickableTitle) this.toggleState();
  };

  render() {
    return (
      <div style={{ ...this.props.style }} className={styles.container}>
        <div
          onClick={this.titleClick}
          className={styles.title}
          style={{
            color: color.accent,
            flexDirection: "row",
            display: "flex",
            alignItems: "center"
          }}
        >
          <div>
            <IconContext.Provider
              value={{
                color: color.accent,
                size: "1.5rem",
                marginRight: "1rem",
                fontWeight: 900
              }}
            >
              {(this.state.isOpen && (
                <div onClick={this.toggleState}>{icon.CHEVRON_CIRCLE_DOWN}</div>
              )) || (
                <div onClick={this.toggleState}>
                  {icon.CHEVRON_CIRCLE_RIGHT}
                </div>
              )}
            </IconContext.Provider>
          </div>
          <div
            style={{
              margin: "0 0 0 .5rem",
              fontWeight: 900,
              position: "relative",
              top: "-.1rem"
            }}
          >
            {this.props.title}
          </div>
        </div>
        <div
          className={this.state.isOpen ? styles.uncollapsed : styles.collapsed}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}
export default Collapsable;

Collapsable.defaultProps = {
  isOpen: false,
  clickableTitle: true
};

Collapsable.propTypes = {
  clickableTitle: PropTypes.bool,
  style: PropTypes.object,
  title: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  isOpen: PropTypes.bool
};
