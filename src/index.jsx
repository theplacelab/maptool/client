// https://stackoverflow.com/questions/70728011/after-updating-to-the-latest-redux-dev-tools-extension-i-am-getting-symbol-obs
// eslint-disable-next-line
import Symbol_observable from "symbol-observable";
import "./index.css";
import React, { useEffect } from "react";
import ReactDOM from "react-dom/client";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "src/redux/store.js";
import { Map as MapDisplay, AlertBar } from "src/components";
import {
  Settings,
  AdminMenu,
  Assets,
  Asset,
  Users,
  User,
  Maps,
  Map,
  Poi,
  PoiField,
  Login,
  Validate
} from "./pages";
import { useReauthorizeMutation } from "./services/auth";
import { setStatus } from "./redux/slice/status";
import { setCredentials, clearCredentials } from "./redux/slice/auth";
import { setDimensions } from "./redux/slice/settings";
import useWindowDimensions from "./hooks/useWindowDimensions";
import { up as authUp } from "./services/auth";
import { up as assetUp } from "./services/asset";
import { up as mapUp } from "./services/map";
import { Error } from "./pages";
import { Spinner } from "./components";

import { setAlert } from "./redux/slice/alert";
import { icon, color } from "./styles.jsx";
import { RequiredPasswordChange, RequiredValidation } from "./pages";

const PrivateRoute = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.auth.claims);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const passChangeRequired = user.f_reset;
  const validationRequired = Object.keys(user).length > 0 && !user.f_valid;

  useEffect(() => {
    dispatch(
      setAlert({
        text:
          validationRequired && isAuthenticated
            ? "Please validate your account"
            : "",
        color: color.alertWarning
      })
    );
  }, [dispatch, passChangeRequired, validationRequired, isAuthenticated]);

  return (
    <React.Fragment>
      {(validationRequired && <RequiredValidation user={user} />) ||
        (passChangeRequired && <RequiredPasswordChange user={user} />) || (
          <React.Fragment>
            <AlertBar />
            {isAuthenticated ? <AdminMenu /> : <Login />}
          </React.Fragment>
        )}
    </React.Fragment>
  );
};

const App = () => {
  const dispatch = useDispatch();
  const serviceOffline = useSelector(
    (state) => !state.status.service.auth.up || !state.status.service.map.up
  );
  const showSpinner = useSelector((state) => state.status.isWorking);

  const { width, height } = useWindowDimensions();
  useEffect(() => {
    dispatch(setDimensions({ width, height }));
  }, [dispatch, width, height]);

  // Reauth on load: attempt to refresh session using RT from localStorage
  const [reauthorize] = useReauthorizeMutation();
  useEffect(() => {
    const refreshToken = localStorage.getItem("REFRESH.TOKEN");
    localStorage.removeItem("REFRESH.TOKEN");
    if (refreshToken) {
      const reauthorizeUser = async () => {
        const res = await reauthorize(refreshToken);
        if (!res.error) dispatch(setCredentials(res.data));
      };
      reauthorizeUser().catch(console.error);
    }
  }, [dispatch, reauthorize]);

  // Token Refresh: Every 45 seconds, attempt to refresh with the token we have
  const refreshToken = useSelector((state) => state.auth.token.refresh);
  useEffect(() => {
    const doRefresh = async () => {
      if (refreshToken) {
        const res = await reauthorize(refreshToken);
        if (!res.error) {
          dispatch(setCredentials(res.data));
        } else {
          dispatch(clearCredentials());
        }
      }
    };
    const timer = setInterval(doRefresh, 45000);
    return () => clearTimeout(timer);
  }, [dispatch, reauthorize, refreshToken]);

  // Service Check: Every 15 seconds, verify all systems go
  useEffect(() => {
    const doUptimeCheck = async () => {
      const auth = await authUp();
      const asset = await assetUp();
      const map = await mapUp();
      dispatch(setStatus({ auth, asset, map }));
    };
    doUptimeCheck();
    const timer = setInterval(doUptimeCheck, 15000);
    return () => clearTimeout(timer);
  }, [dispatch]);

  // Routing
  return (
    <React.Fragment>
      {showSpinner && <Spinner />}
      {serviceOffline && (
        <Error
          message={"Service Unreachable"}
          subMessage={"Check your network connection"}
          icon={icon.OFFLINE}
        />
      )}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MapDisplay />} />
          <Route path="/:map_slug" element={<MapDisplay />} />
          <Route path="/:map_slug/:poi_slug" element={<MapDisplay />} />

          <Route
            path="maptool/user/validate/:validationKey"
            element={<Validate />}
          />
          <Route path="maptool/:email" element={<PrivateRoute />} />
          <Route path="maptool" element={<PrivateRoute />}>
            <Route path="" element={<MapDisplay embedded={true} />} />
            <Route
              path="preview/:map_slug"
              element={<MapDisplay embedded={true} />}
            />
            <Route
              path="preview/:map_slug/:poi_slug"
              element={<MapDisplay embedded={true} />}
            />
            <Route path="preview" element={<MapDisplay embedded={true} />} />
            <Route path="settings" element={<Settings />} />
            <Route path="assets" element={<Assets />} />
            <Route path="asset/:id" element={<Asset />} />
            <Route path="users" element={<Users />} />
            <Route path="user/:id" element={<User />} />
            <Route path="maps" element={<Maps />} />
            <Route path="map/:id/poi/:poi_id" element={<Poi />} />
            <Route path="map/:id/poi/config/:poi_id" element={<PoiField />} />
            <Route path="map/:id" element={<Map />} />
          </Route>
          <Route path="*" element={<Map />} />
        </Routes>
      </BrowserRouter>{" "}
    </React.Fragment>
  );
};

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
