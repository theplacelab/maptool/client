import jwt_decode from "jwt-decode";
import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  isAuthenticated: false,
  token: {
    auth: "",
    refresh: ""
  },
  claims: {}
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setCredentials: (state, { payload: { auth, refresh } }) => {
      if (!auth || !refresh) return initialState;
      state.isAuthenticated = auth.length > 0;
      state.token.auth = auth;
      state.token.refresh = refresh;
      if (auth.length > 0) {
        state.claims = jwt_decode(auth);
      } else {
        state.claims = {};
      }
    },
    clearCredentials: () => {
      return initialState;
    }
  }
});

export const { setCredentials, clearCredentials } = authSlice.actions;
export default authSlice.reducer;
