import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  isWorking: false,
  service: {
    auth: { up: true },
    asset: { up: true },
    map: { up: true }
  }
};

export const statusSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setStatus: (state, { payload: { auth, asset, map } }) => {
      state.service.auth.up = auth;
      state.service.asset.up = asset;
      state.service.map.up = map;
    }
  }
});

export const { setStatus } = statusSlice.actions;
export default statusSlice.reducer;
