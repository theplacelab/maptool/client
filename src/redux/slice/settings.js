import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  width: 0,
  height: 0,
  compact: false
};

export const settingsSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setDimensions: (state, { payload: { width, height } }) => {
      state.width = width;
      state.height = height;
      state.compact = width <= 700;
    }
  }
});

export const { setDimensions } = settingsSlice.actions;
export default settingsSlice.reducer;
