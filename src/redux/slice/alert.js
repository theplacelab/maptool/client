import { createSlice } from "@reduxjs/toolkit";
import { color } from "src/styles.jsx";

const initialState = {
  text: "",
  color: color.alertError,
  expiresAfter: null
};

export const alertSlice = createSlice({
  name: "alert",
  initialState,
  reducers: {
    setAlert: (state, action) => {
      state.text = action.payload.text;
      state.expiresAfter = action.payload.expiresAfter
        ? action.payload.expiresAfter
        : null;
      if (action.payload.color) state.color = action.payload.color;
    },
    setWarning: (state, action) => {
      state.text = action.payload;
      state.color = color.alertWarning;
    },
    clearAlert: (state) => {
      state.text = "";
    }
  }
});

export const { setWarning, setAlert, clearAlert } = alertSlice.actions;
export default alertSlice.reducer;
