import { configureStore, createListenerMiddleware } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { authApi } from "../services/auth";
import { assetApi } from "../services/asset";
import { mapApi } from "../services/map";
import alertReducer from "./slice/alert";
import settingsReducer from "./slice/settings";
import statusReducer from "./slice/status";
import authReducer, { setCredentials, clearCredentials } from "./slice/auth";
const listenerMiddleware = createListenerMiddleware();

listenerMiddleware.startListening({
  actionCreator: setCredentials,
  effect: async (action, listenerApi) => {
    const { auth } = listenerApi.getState();
    localStorage.setItem("REFRESH.TOKEN", auth.token.refresh);
  }
});

listenerMiddleware.startListening({
  actionCreator: clearCredentials,
  effect: () => {
    localStorage.removeItem("REFRESH.TOKEN");
  }
});

export const store = configureStore({
  reducer: {
    alert: alertReducer,
    auth: authReducer,
    settings: settingsReducer,
    status: statusReducer,
    [authApi.reducerPath]: authApi.reducer,
    [assetApi.reducerPath]: assetApi.reducer,
    [mapApi.reducerPath]: mapApi.reducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
      .concat(authApi.middleware)
      .concat(assetApi.middleware)
      .concat(mapApi.middleware)
      .prepend(listenerMiddleware.middleware)
});
setupListeners(store.dispatch);
