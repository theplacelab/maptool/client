import React from "react";
import { default as useStyles } from "src/styles.jsx";
import { Button, Avatar, StatusIcon } from "src/components";
import { color, icon } from "src/styles.jsx";
import { useUpdateUserMutation } from "src/services/auth";
import { useSelector, useDispatch } from "react-redux";
import { resetUser } from "src/services/auth";
import { setAlert } from "src/redux/slice/alert";

const AvatarEdit = ({ setDialog, id, user }) => {
  const dispatch = useDispatch();
  const compactView = useSelector((state) => state.settings.compact);
  const myId = parseInt(
    useSelector((state) => state.auth.claims.id),
    10
  );

  const [updateUser] = useUpdateUserMutation();

  const classes = useStyles();

  const displayDialog = (opts) => {
    setDialog({
      ...opts,
      isVisible: true,
      onCancel: () => setDialog({ isVisible: false })
    });
  };

  const toggleAdmin = () => {
    if (user.uuid === myId) return;
    if (user?.role === "websuper") {
      displayDialog({
        onConfirm: () => {
          updateUser({ uuid: user.uuid, role: "webuser" });
          setDialog({ isVisible: false });
        },
        title: "Demote?",
        message:
          "Do you want to demote this user from admin?\n They will lose access to user management."
      });
    } else {
      displayDialog({
        onConfirm: () => {
          updateUser({ uuid: user.uuid, role: "websuper" });
          setDialog({ isVisible: false });
        },
        title: "Promote?",
        message:
          "Do you want to promote this user to admin?\n They will gain access to user management."
      });
    }
  };

  const sendValidationRequest = () => {
    if (!user?.f_valid) {
      displayDialog({
        onConfirm: () => {
          resetUser(user.email);
          setDialog({ isVisible: false });
          dispatch(
            setAlert({
              text: `Password has been reset for ${user.name} (${user.email})`,
              color: color.inlineOk
            })
          );
        },
        title: "Reset Password?",
        message:
          "Do you want to reset the password for this account? The account will be locked until the user resets their password.",
        confirmLabel: "Yes",
        cancelLabel: "No"
      });
    }
  };

  return (
    <div className={classes.panel}>
      <div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div style={styles.avatarFrame}>
            <Avatar user={user} />
          </div>
          <div style={styles.container}>
            {user?.role === "websuper" && (
              <StatusIcon
                active={user?.role === "websuper"}
                text={
                  compactView
                    ? ""
                    : user?.role === "websuper"
                    ? "Admin"
                    : "User"
                }
                icon={icon.USER.ADMIN}
                onClick={toggleAdmin}
              />
            )}
            <StatusIcon
              active={user?.f_valid}
              text={
                compactView ? "" : user?.f_valid ? "Confirmed" : "Not Confirmed"
              }
              icon={icon.USER.VALID}
            />
            <div style={{ flexGrow: 1 }} />
            <div>
              <Button
                type="button"
                style={styles.smallButton}
                onClick={sendValidationRequest}
                label="reset"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const styles = {
  avatarFrame: {
    position: "relative",
    textAlign: "center",
    margin: "auto",
    width: "100%",
    background: color.backgroundTint3,
    padding: "1rem",
    boxSizing: "border-box"
  },
  container: {
    display: "flex",
    alignItems: "center",
    background: color.backgroundTint2,
    padding: ".3rem 1rem",
    overflow: "hidden",
    marginBottom: "2rem"
  },
  smallButton: {
    userSelect: "none",
    minWidth: 0,
    height: "1rem",
    fontSize: "1rem",
    lineHeight: 0,
    padding: "1rem",
    margin: ".2rem"
  }
};

export default AvatarEdit;
