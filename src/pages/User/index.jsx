import React from "react";
import { AddEdit } from "../Common/";
import Add from "./Add";
import Edit from "./Edit";

const UserAddEdit = () => {
  return <AddEdit cancelTo={`../users`} title="Users" Add={Add} Edit={Edit} />;
};

export default UserAddEdit;
