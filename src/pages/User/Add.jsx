import React, { useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Input } from "src/components/Form";
import { Button, InlineMessage } from "src/components";
import { color } from "src/styles.jsx";
import { useCreateUserMutation } from "src/services/auth";
import { useNavigate } from "react-router-dom";

const Add = () => {
  const navigate = useNavigate();
  const classes = useStyles();
  const [message, setMessage] = useState({ text: "" });
  const [createUser] = useCreateUserMutation();
  const [formValues, setFormValues] = useState({});

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();

    if (Object.keys(formValues).length > 1) {
      if (formValues.email && formValues.email.trim().length > 0) {
        const { data, error } = await createUser(formValues);
        if (error) {
          let text = JSON.stringify(error);
          if (error.status === 409) {
            text = "Email already in use";
          }
          setMessage({
            text,
            background: color.inlineError,
            color: color.inlineErrorText
          });
        } else {
          navigate(`/maptool/user/${data.uuid}`);
        }
      } else {
        setMessage({
          text: "Email cannot be blank",
          background: color.inlineError,
          color: color.inlineErrorText
        });
      }
    }
  };

  const onChange = (e) => {
    setFormValues({ ...formValues, [e.target.id]: e.target.value?.trim() });
  };

  return (
    <form onSubmit={onSubmit} method="POST">
      <div className={classes.panel}>
        <div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {message.text && <InlineMessage message={message} />}
            <div>
              <Input
                id="email"
                type="text"
                autoComplete="email"
                onChange={onChange}
                label="Email"
                value={formValues.email}
                placeholder="basilisk@example.com"
              />
            </div>
            <div>
              <Input
                id="name"
                type="text"
                autoComplete="name"
                onChange={onChange}
                label="Name"
                value={formValues.name}
                placeholder=""
              />
            </div>
            <div>
              <Input
                id="avatar"
                type="text"
                autoComplete="avatar"
                onChange={onChange}
                label="avatar"
                value={formValues.avatar}
                placeholder=""
              />
            </div>
            <div className={classes.upperRightCta}>
              <Button label={"Create"} className={classes.smallButton} />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Add;
