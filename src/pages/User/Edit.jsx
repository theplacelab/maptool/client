import React, { useState, useEffect } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Input } from "src/components/Form";
import { Button, InlineMessage } from "src/components";
import { color } from "src/styles.jsx";
import {
  useUpdateUserMutation,
  useDeleteUserMutation
} from "src/services/auth";
import { useNavigate } from "react-router-dom";
import AvatarEdit from "./AvatarEdit";
import { useGetUserByIdQuery } from "src/services/auth";
import UserPasswordChange from "src/pages/UserPasswordChange";
import { useSelector } from "react-redux";

const Edit = ({ id, setDialog }) => {
  const navigate = useNavigate();
  const { data: user } = useGetUserByIdQuery(id);
  const classes = useStyles();
  const [message, setMessage] = useState({ text: "" });
  const [updateUser] = useUpdateUserMutation();
  const [deleteUser] = useDeleteUserMutation();
  const [formValues, setFormValues] = useState({ id });

  const displayDialog = (opts) => {
    setDialog({
      ...opts,
      isVisible: true,
      onCancel: () => setDialog({ isVisible: false })
    });
  };

  const myUuid = useSelector((state) => state.auth.claims.uuid);

  useEffect(() => {
    if (user)
      setFormValues({
        uuid: user.uuid,
        email: user.email,
        name: user.name,
        avatar: user.avatar
      });
  }, [user, id]);

  const onDeleteUser = () => {
    displayDialog({
      onConfirm: () => {
        deleteUser({ uuid: user.uuid });
        navigate(`/maptool/users`);
      },
      title: "Delete User?",
      message:
        "Do you want to delete this user?\n This action cannot be reversed!"
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();
    if (Object.keys(formValues).length > 1) {
      if (user) {
        // Edit
        const { error } = await updateUser(formValues);
        if (error) {
          console.log(error);
          setMessage({ text: "That email is already in use" });
          setFormValues({ ...formValues, email: user.email });
        } else {
          setMessage({
            text: "Account updated!",
            background: color.inlineOk,
            color: color.inlineOkText
          });
        }
      }
    }
  };

  const onChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.id]:
        typeof e.target.value === "string"
          ? e.target.value.trim()
          : e.target.value
    });
  };

  return (
    <React.Fragment>
      <AvatarEdit setDialog={setDialog} id={id} user={user} />

      <form onSubmit={onSubmit} method="POST">
        <div className={classes.panel}>
          <div>
            <div style={{ display: "flex", flexDirection: "column" }}>
              {message.text && <InlineMessage message={message} />}
              <div>
                <Input
                  id="email"
                  type="text"
                  autoComplete="email"
                  onChange={onChange}
                  label="Email"
                  value={formValues.email}
                  placeholder="basilisk@example.com"
                />
              </div>
              <div>
                <Input
                  id="name"
                  type="text"
                  autoComplete="name"
                  onChange={onChange}
                  label="Name"
                  value={formValues.name}
                  placeholder=""
                />
              </div>
              <div>
                <Input
                  id="avatar"
                  type="text"
                  autoComplete="avatar"
                  onChange={onChange}
                  label="avatar"
                  value={formValues.avatar}
                  placeholder=""
                />
              </div>
              <div style={{ color: color.backgroundTint2 }}>
                <div>{user?.last_access}</div>
              </div>
              <div>
                <div style={{ fontSize: "0.5rem", whiteSpace: "nowrap" }}>
                  {user?.uuid}
                </div>
                <div style={{ fontSize: "0.5rem", whiteSpace: "nowrap" }}>
                  {user?.created_at}
                </div>
              </div>
              <div className={classes.upperRightCta}>
                <Button
                  type="button"
                  label="delete"
                  className={classes.smallButton}
                  onClick={onDeleteUser}
                />
                <Button label={"Save"} className={classes.smallButton} />
              </div>
            </div>
          </div>
        </div>
      </form>

      {id === myUuid && (
        <div>
          <div className={classes.horizontalRule} />
          <UserPasswordChange user={user} />
        </div>
      )}
    </React.Fragment>
  );
};

export default Edit;
