import React from "react";
import { IconContext } from "react-icons";
import { color } from "src/styles.jsx";
import { useSelector } from "react-redux";
import { IndicatorLamp } from "src/components";

export default function Error({
  embedded = false,
  backgroundColor = null,
  message,
  subMessage,
  icon
}) {
  const systemStatus = useSelector((state) => state.status);
  const compactView = useSelector((state) => state.settings.compact);
  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        height: "100%",
        width: embedded
          ? compactView
            ? "100vw"
            : "calc(100vw - 17rem)"
          : "100vw",
        background: backgroundColor ? backgroundColor : "rgb(0 0 0 / 75%)",
        zIndex: 5,
        display: "flex",
        textAlign: "center",
        overflow: "hidden"
      }}
    >
      <div style={{ margin: "auto", color: color.background, fontWeight: 900 }}>
        <IconContext.Provider
          value={{
            color: color.accent,
            size: "5rem"
          }}
        >
          {icon}
        </IconContext.Provider>
        <div style={{ textAlign: "center" }}>
          <IndicatorLamp
            label="Auth Service"
            up={systemStatus.service.auth.up}
          />
          <IndicatorLamp label="Map Service" up={systemStatus.service.map.up} />
          <IndicatorLamp
            label="Asset Service"
            up={systemStatus.service.asset.up}
          />
        </div>
        <div style={{ marginTop: "1rem" }}>{message}</div>
        <div style={{ marginTop: ".2rem" }}>{subMessage}</div>
      </div>
    </div>
  );
}
