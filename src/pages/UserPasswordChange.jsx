import React, { useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Input } from "src/components/Form";
import { Button, InlineMessage } from "src/components";
import { color } from "src/styles.jsx";
import { useChangePasswordMutation } from "src/services/auth";

const UserPasswordChange = ({ user, forcedChange = false }) => {
  const [changePassword] = useChangePasswordMutation();
  const [message, setMessage] = useState({ text: "" });
  const [formValues, setFormValues] = useState({
    id: user?.id,
    email: user?.email,
    pass: "",
    newPass: "",
    newPassConfirm: ""
  });
  const classes = useStyles();
  const onChange = (e) => {
    setFormValues({ ...formValues, [e.target.id]: e.target.value?.trim() });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();
    if (formValues.newPass.trim().length === 0) {
      setMessage({ text: "New password cannot be blank" });
    } else if (formValues.pass === formValues.newPass) {
      setMessage({
        text: "New password cannot be the same as the old password"
      });
    } else if (formValues.newPass === formValues.newPassConfirm) {
      console.log(formValues);
      await changePassword(formValues)
        .unwrap()
        .then(() => {
          if (forcedChange) {
            window.location.reload();
          } else {
            setMessage({
              text: "Password Changed",
              color: color.inlineOkText,
              background: color.inlineOk
            });
          }
        })
        .catch(() => setMessage({ text: "Current password is incorrect" }));
    } else {
      setMessage({ text: "New password doesn't match confirmation" });
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <div className={classes.panel}>
        <div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {message.text && <InlineMessage message={message} />}
            <div>
              <Input
                id="pass"
                type="password"
                autoComplete="password"
                onChange={onChange}
                value={formValues.pass}
                label="Current Password"
                placeholder="••••••••••••••"
              />
            </div>
            <div>
              <Input
                id="newPass"
                type="password"
                value={formValues.newPass}
                autoComplete="password"
                onChange={onChange}
                label="New Password"
                placeholder="••••••••••••••"
              />
            </div>
            <div>
              <Input
                id="newPassConfirm"
                type="password"
                value={formValues.newPassConfirm}
                autoComplete="password"
                onChange={onChange}
                label="Confirm New Password"
                placeholder="••••••••••••••"
              />
            </div>
            <div style={{ display: "flex", marginTop: "2rem" }}>
              <Button
                label="Update Password"
                style={{ margin: "0 0 0 auto" }}
              />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default UserPasswordChange;
