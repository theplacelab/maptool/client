import { useDispatch, useSelector } from "react-redux";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { default as useStyles } from "src/styles.jsx";
import { useLoginMutation } from "src/services/auth";
import { setAlert, clearAlert } from "src/redux/slice/alert";
import { setCredentials, clearCredentials } from "src/redux/slice/auth";
import { Button, IndicatorLamp } from "src/components";
import { Input } from "src/components/Form";
import { resetUser } from "src/services/auth";
import { color } from "src/styles.jsx";

export default function Login() {
  const params = useParams();
  const dispatch = useDispatch();
  const classes = useStyles();
  const [login] = useLoginMutation();
  const systemStatus = useSelector((state) => state.status);

  const forgotPassword = async () => {
    if (formValues.email?.trim().length > 0) {
      const resetStatus = await resetUser(formValues.email?.trim());
      dispatch(setAlert(resetStatus));
    }
  };

  const [formValues, setFormValues] = useState({ email: params.email });

  if (params.email) window.history.replaceState(null, "Login", "/maptool");

  const onChange = (e) => {
    setFormValues({ ...formValues, [e.target.id]: e.target.value?.trim() });
  };
  const onSubmit = async (e) => {
    e.preventDefault();
    dispatch(clearAlert());
    dispatch(clearCredentials());
    if (
      formValues.email?.trim().length > 0 &&
      formValues.password?.trim().length > 0
    ) {
      try {
        const user = await login({
          email: formValues.email.trim(),
          pass: formValues.password.trim()
        });
        if (user.error) {
          console.error(user.error);
          setFormValues({});
        } else {
          dispatch(setCredentials(user.data));
        }
      } catch (err) {
        const text =
          parseInt(err.status, 10) === 401
            ? "Incorrect username or password"
            : err.status;
        dispatch(setAlert({ text, color: color.alertError }));
      }
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <div className={classes.container}>
        <div
          style={{
            margin: "auto",
            padding: "1rem",
            width: "30rem",
            maxWidth: "40rem",
            minWidth: "300px"
          }}
        >
          <div className={classes.title} style={{ textAlign: "center" }}>
            Sign in to Maptool
          </div>
          <div style={{ textAlign: "center" }}>
            <IndicatorLamp
              label="Auth Service"
              up={systemStatus.service.auth.up}
            />
            <IndicatorLamp
              label="Map Service"
              up={systemStatus.service.map.up}
            />
            <IndicatorLamp
              label="Asset Service"
              up={systemStatus.service.asset.up}
            />
          </div>
          <div>
            <Input
              id="email"
              type="text"
              autoComplete="email"
              onChange={onChange}
              label="Email"
              value={formValues.email}
              placeholder="basilisk@example.com"
            />
          </div>
          <div>
            <Input
              id="password"
              type="password"
              autoComplete="current-password"
              onChange={onChange}
              label="Password"
              value={formValues.password}
              placeholder="•••••••••"
              overlay={
                <div style={{ opacity: 0.5 }} onClick={forgotPassword}>
                  Forgot?
                </div>
              }
            />
            <Button style={{ marginTop: ".5rem" }} label="Sign In" />
          </div>
        </div>
      </div>
    </form>
  );
}
