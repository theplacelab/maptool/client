import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Outlet, useLocation } from "react-router-dom";
import { IconContext } from "react-icons";

import { color, font, icon } from "src/styles.jsx";
import { clearCredentials } from "src/redux/slice/auth";
import { User } from "src/components";
import { useNavigate } from "react-router-dom";

const MenuItem = ({ compactView, icon, currentSelection, linkTo, label }) => {
  const navigate = useNavigate();
  let location = useLocation();
  const selected = location?.pathname.includes(linkTo);

  return (
    <IconContext.Provider
      value={{
        color: selected ? color.accent : color.foreground,
        size: "1.5rem"
      }}
    >
      <div
        onClick={() => navigate(linkTo)}
        style={{ ...styles.menuItem, ...styles }}
      >
        {!compactView && <div style={styles.handle}></div>}
        <div style={styles.icon}>{icon}</div>
        <div
          style={{
            ...styles.label,
            color: selected ? color.accent : color.foreground,
            fontWeight: selected ? 900 : 400
          }}
        >
          {label}
        </div>
      </div>
    </IconContext.Provider>
  );
};

export default function Menu() {
  const compactView = useSelector((state) => state.settings.compact);
  const userInfo = useSelector((state) => state.auth.claims);
  const dispatch = useDispatch();
  let location = useLocation();
  const currentSelection = location?.pathname.split("/").pop();
  return (
    <div style={styles.flexRow}>
      <div style={!compactView ? styles.menuSide : styles.menuBottom}>
        {(compactView && <div style={styles.flexSpacer}></div>) || (
          <div style={styles.header}>
            <div style={styles.headerIcon}>
              <IconContext.Provider
                value={{ color: "#bbbbbb", size: "2.5rem" }}
              >
                {icon.MAP_OUTLINE}
              </IconContext.Provider>
            </div>
            <div style={styles.headerTitle}>Maptool</div>
            <div />
          </div>
        )}

        {!compactView && (
          <div
            style={{
              marginBottom: ".5rem",
              background: color.backgroundTint3
            }}
          >
            <MenuItem
              compactView={compactView}
              currentSelection={currentSelection}
              linkTo={"/maptool/preview"}
              label={"Preview"}
              icon={icon.SECTION.PREVIEW}
            />
          </div>
        )}

        <MenuItem
          compactView={compactView}
          currentSelection={currentSelection}
          linkTo={"/maptool/maps"}
          label={"Maps"}
          icon={icon.SECTION.MAPS}
        />

        <MenuItem
          compactView={compactView}
          currentSelection={currentSelection}
          linkTo={"/maptool/assets"}
          label={"Assets"}
          icon={icon.SECTION.ASSETS}
        />

        {userInfo.role === "websuper" && (
          <MenuItem
            compactView={compactView}
            currentSelection={currentSelection}
            linkTo={"/maptool/users"}
            label={"Users"}
            icon={icon.SECTION.USERS}
          />
        )}

        <div style={styles.flexSpacer}></div>

        {!compactView && (
          <div
            style={{ display: "flex", alignItems: "center", padding: "1rem" }}
          >
            <div style={{ flexGrow: 1 }}>
              {(userInfo.uuid && (
                <User
                  id={userInfo.uuid}
                  onLogout={() => dispatch(clearCredentials())}
                />
              )) || <div>Loading...</div>}
            </div>
            <div
              style={{
                width: "3rem",
                height: "3rem",
                lineHeight: "3.8rem",
                textAlign: "center",
                borderRadius: "2rem",
                background: color.backgroundTint3
              }}
            >
              <IconContext.Provider
                value={{ color: color.accent, size: "1.5rem" }}
              >
                <Link style={styles.link} to="settings">
                  {icon.SECTION.SETTINGS}
                </Link>
              </IconContext.Provider>
            </div>
          </div>
        )}
      </div>
      <div
        style={{ ...styles.mainPanel, marginLeft: !compactView ? "17rem" : 0 }}
      >
        <Outlet />
      </div>
    </div>
  );
}

const styles = {
  link: {
    textDecoration: "none",
    color: "#000000"
  },
  flexRow: {
    display: "flex",
    flexDirection: "row"
  },
  flexSpacer: { flexGrow: 1 },
  header: {
    background: color.backgroundTint2,
    padding: "1rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  headerTitle: {
    fontWeight: 900,
    marginLeft: "1rem",
    fontFamily: font.display,
    fontSize: "2.3rem",
    color: color.accent
  },
  label: { textAlign: "left", flexGrow: 1, marginLeft: ".5rem" },
  menuItem: {
    padding: "1rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    width: "2rem",
    textAlign: "center",
    position: "relative",
    top: 2
  },
  headerIcon: {
    width: "2.4rem",
    textAlign: "center",
    position: "relative",
    top: 2,
    left: 5
  },
  handle: { width: "1rem" },
  mainPanel: {
    margin: "0 auto 0 auto",
    width: "100vw",
    minHeight: "calc(100vh - 4rem)",
    overflow: "scroll",
    padding: 0
  },
  settingsIcon: {
    width: "100%",
    textAlign: "right",
    margin: "0 -1rem 1rem"
  },
  menuSide: {
    userSelect: "none",
    position: "fixed",
    display: "flex",
    flexDirection: "column",
    background: color.backgroundTint1,
    width: "17rem",
    minWidth: "17rem",
    height: "100vh",
    borderRight: "1px solid #00000012"
  },
  menuBottom: {
    userSelect: "none",
    zIndex: 2,
    display: "flex",
    position: "fixed",
    bottom: 0,
    flexDirection: "row",
    background: "#eeeeee",
    width: "100vw",
    maxHeight: "4rem",
    borderTop: "1px solid #00000012"
  }
};
