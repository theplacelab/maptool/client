import React, { useState } from "react";
import { color } from "src/styles.jsx";
import { useNavigate } from "react-router-dom";
import { useCreatePoiMutation } from "src/services/map";
import { InlineMessage, Button } from "src/components";
import Form from "./Form";
import { default as useStyles } from "src/styles.jsx";

const Add = ({ mapId }) => {
  const navigate = useNavigate();
  const [createPoi] = useCreatePoiMutation();
  const [message, setMessage] = useState();
  const [formValues, setFormValues] = useState({});
  const classes = useStyles();
  const onChange = (e) => {
    setFormValues({
      ...formValues,
      [Number(e.target.id) ? Number(e.target.id) : e.target.id]: e.target.value
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();
    if (Object.keys(formValues).length > 0) {
      if (formValues.title && formValues.title.trim().length > 0) {
        const { error } = await createPoi({
          mapId,
          newPoi: formValues
        });
        if (error) {
          setMessage({
            text: JSON.stringify(error),
            background: color.inlineError,
            color: color.inlineErrorText
          });
        } else {
          navigate(`/maptool/map/${mapId}`);
        }
      } else {
        setMessage({
          text: "Poi title cannot be blank",
          background: color.inlineError,
          color: color.inlineErrorText
        });
      }
    }
  };

  return (
    <>
      {message?.text && <InlineMessage message={message} />}
      <Form
        mapId={mapId}
        formValues={formValues}
        onChange={onChange}
        onSubmit={onSubmit}
        cta={
          <>
            <Button
              onClick={() => navigate(`/maptool/map/${mapId}`)}
              label={"Cancel"}
              className={classes.smallButton}
            />
            <Button label={"Add"} className={classes.smallButton} />
          </>
        }
      />
    </>
  );
};

export default Add;
