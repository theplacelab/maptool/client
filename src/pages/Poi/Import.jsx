import React, { useState } from "react";
import { color, font } from "src/styles.jsx";
import { InlineMessage, Button } from "src/components";
import { default as useStyles } from "src/styles.jsx";
import {
  useGetPoiDataFormatQuery,
  useGetPoiFieldQuery
} from "src/services/map";
import { Input, FIELD_TYPE } from "src/components/Form";
//import PoiDetail from "src/components/Map/PoiDetail";
// <PoiDetail mapId={mapId} data={sample} />

const Import = ({ mapId, data }) => {
  const [message] = useState();
  const [formValues, setFormValues] = useState({
    dry_run: true,
    skip_duplicates: true
  });
  const [examplePointer, onSetExamplePointer] = useState(0);
  const { data: poiFields } = useGetPoiFieldQuery(mapId);
  const classes = useStyles();

  const setExamplePointer = (pointer) => {
    if (pointer < 0) {
      onSetExamplePointer(data.data.length - 1);
    } else if (pointer >= data.data.length) {
      onSetExamplePointer(0);
    } else {
      onSetExamplePointer(pointer);
    }
  };

  const onChange = (e) => {
    const newFormValues = {
      ...formValues,
      [e.target.id]: e.target.value
    };
    if (e.target.value === "-1") delete newFormValues[e.target.id];
    setFormValues(newFormValues);
    console.log(newFormValues);
  };

  const onSubmit = () => {
    const dryRun = formValues.dry_run;
    const skipDuplicates = formValues.skip_duplicates;
    const mapping = structuredClone(formValues);
    delete mapping.dry_run;
    delete mapping.skip_duplicates;

    console.log(`DRY: ${formValues.dry_run}`);
    console.log(`DEDUPE: ${formValues.skip_duplicates}`);
    data.data.forEach((row) => {
      console.log(row.name);

      Object.keys(mapping).forEach((i) => {
        console.log(
          `${i} ---> ${mapping[i]} (${
            poiFields.find((j) => j.id === Number(mapping[i]))?.label
          })`
        );
      });

      /*
      if(!dryRun)
            const { error } = await createPoi({
          mapId,
          newPoi: formValues
        });*/
    });
  };

  const sample = data.data[examplePointer];
  const fields = [
    ...new Set(
      data.data.map((row) => row.fields.map((field) => field.field)).flat()
    )
  ];

  let fieldOptions = poiFields?.map((i) => ({
    display_name: i.label,
    id: i.id
  }));
  const defaultOptions = [
    { display_name: "- - -", id: -1 },
    { display_name: "Coordinates", id: "coordinates" },
    { display_name: "Name", id: "name" }
  ];
  fieldOptions = [...defaultOptions, ...fieldOptions];

  return (
    <>
      {message?.text && <InlineMessage message={message} />}
      <div style={styles.row}>
        <div
          style={{
            flexGrow: 1,
            fontFamily: font.display,
            fontSize: "1.5rem",
            color: color.accent2
          }}
        >
          <b>{data.data.length}</b> POI
        </div>
        <div style={{ ...styles.row }}>
          <Input
            type={FIELD_TYPE.CHECKBOX}
            value={formValues.dry_run}
            label="Dry Run"
            id="dry_run"
            onChange={onChange}
          />
          <Input
            type={FIELD_TYPE.CHECKBOX}
            value={formValues.skip_duplicates}
            label="Skip Duplicates"
            id="skip_duplicates"
            onChange={onChange}
          />
          <Button
            onClick={() => window.location.reload()}
            label={"Cancel"}
            className={classes.smallButton}
          />
          <Button
            onClick={onSubmit}
            label={"Import"}
            className={classes.smallButton}
          />
        </div>
      </div>

      <br />
      <br />
      <div>
        <div style={styles.header}>
          <div style={styles.header2}>Data ➙ Field</div>
          <Button
            className={classes.smallButton}
            label="< Prev"
            onClick={() => setExamplePointer(examplePointer - 1)}
          />
          <div style={{ flexGrow: 1, textAlign: "center", fontWeight: 900 }}>
            {examplePointer + 1}/{data.data.length}
          </div>
          <Button
            className={classes.smallButton}
            label="Next >"
            onClick={() => setExamplePointer(examplePointer + 1)}
          />
        </div>
        <div style={styles.mapping}>
          <div style={{ backgroundColor: "white" }}>
            <Input
              label={`"Name" to:`}
              type={FIELD_TYPE.SELECT}
              id={"name"}
              options={fieldOptions
                .map((fo) =>
                  String(fo.id) === formValues["name"] ||
                  !Object.values(formValues).includes(String(fo.id))
                    ? fo
                    : null
                )
                .filter((n) => n !== null)}
              onChange={onChange}
              value={formValues["name"]}
            />
          </div>
          <div style={styles.sampleText}> {JSON.stringify(sample.name)}</div>
        </div>
        <div style={styles.mapping}>
          <div style={{ backgroundColor: "white" }}>
            <Input
              label={`"Coordinates" to:`}
              type={FIELD_TYPE.SELECT}
              id={"coordinates"}
              options={fieldOptions
                .map((fo) =>
                  String(fo.id) === formValues["coordinates"] ||
                  !Object.values(formValues).includes(String(fo.id))
                    ? fo
                    : null
                )
                .filter((n) => n !== null)}
              onChange={onChange}
              value={formValues["coordinates"]}
            />
          </div>
          <div style={styles.sampleText}>
            {" "}
            {JSON.stringify(sample.coordinates)}
          </div>
        </div>
        <div style={styles.mapping}>
          <div style={{ backgroundColor: "white" }}>
            <Input
              label={`"POI Date" to:`}
              type={FIELD_TYPE.SELECT}
              id={"date"}
              options={fieldOptions
                .map((fo) =>
                  String(fo.id) === formValues["date"] ||
                  !Object.values(formValues).includes(String(fo.id))
                    ? fo
                    : null
                )
                .filter((n) => n !== null)}
              onChange={onChange}
              value={formValues["date"]}
            />
          </div>
          <div style={styles.sampleText}>{sample.date}</div>
        </div>
        {fields.map((field, idx) => {
          const id = field.replace(" ", "_").toLowerCase();
          return (
            <div style={styles.mapping} key={idx}>
              <div style={{ backgroundColor: "white", minWidth: "16rem" }}>
                <Input
                  id={id}
                  label={`"${field}" to: | ${id}`}
                  type={FIELD_TYPE.SELECT}
                  options={fieldOptions
                    .map((fo) =>
                      String(fo.id) === formValues[id] ||
                      !Object.values(formValues).includes(String(fo.id))
                        ? fo
                        : null
                    )
                    .filter((n) => n !== null)}
                  value={formValues[id]}
                  onChange={onChange}
                />
              </div>
              <div style={styles.sampleText}>
                {sample.fields.find((i) => i?.field === field)?.field_value}
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Import;

const styles = {
  box: { width: "100%", alignItems: "center" },
  header2: {
    textAlign: "center",
    fontWeight: 900,
    alignContent: "center",
    paddingLeft: "1rem",
    width: "15.2rem",
    height: "3rem",
    background: color.accent2,
    color: color.background,
    font: color.font,
    marginRight: "1rem",
    borderRadius: "0.3rem 0.3rem 0 0"
  },
  header: {
    borderRadius: "0.3rem 0.3rem 0 0",
    display: "flex",
    flexDirection: "row",
    backgroundColor: color.backgroundTint3,
    padding: ".5rem",
    alignItems: "center"
  },
  row: {
    display: "flex",
    flexDirection: "row",
    padding: ".5rem",
    alignItems: "center"
  },
  bold: { fontWeight: 900, width: "10rem" },
  sampleText: {
    maxHeight: "5rem",
    overflow: "hidden",
    textOverflow: "ellipsis",
    paddingLeft: "1rem"
  },
  mapping: {
    display: "flex",
    flexDirection: "row",
    padding: ".5rem",
    alignItems: "center",
    backgroundColor: color.backgroundTint3
  }
};
