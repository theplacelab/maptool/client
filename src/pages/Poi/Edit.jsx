import React, { useEffect, useState } from "react";
import { color } from "src/styles.jsx";
import { useNavigate } from "react-router-dom";
import { useUpdatePoiMutation, useGetPoiByIdQuery } from "src/services/map";
import { InlineMessage, Button } from "src/components";
import Form from "./Form";
import { default as useStyles } from "src/styles.jsx";

const Edit = ({ id, mapId }) => {
  const navigate = useNavigate();
  const [updatePoi] = useUpdatePoiMutation();
  const { data: poiData } = useGetPoiByIdQuery({ mapId, poiId: id });
  const [message, setMessage] = useState();
  const [formValues, setFormValues] = useState();
  const classes = useStyles();

  useEffect(() => {
    let values = {};
    const pd = poiData ? poiData[0] : null;
    if (pd) {
      for (const key in pd) {
        if (key !== "fields") values[key] = pd[key];
      }
      pd["fields"].forEach((field) => {
        const id = field.poi_data_options_id;
        values[id] = field.value;
        values[`${id}_markdown`] = field.is_markdown;
        values[`${id}_published`] = field.is_published;
        values[`${id}_locked`] = field.is_locked;
      });
      setFormValues(values);
    }
  }, [poiData]);

  const onChange = (e) => {
    setFormValues({
      ...formValues,
      [Number(e.target.id) ? Number(e.target.id) : e.target.id]: e.target.value
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();
    if (Object.keys(formValues).length > 0) {
      if (formValues.title && formValues.title.trim().length > 0) {
        const { error } = await updatePoi({
          mapId,
          poiData: formValues
        });

        if (error) {
          setMessage({
            text: JSON.stringify(error),
            background: color.inlineError,
            color: color.inlineErrorText
          });
        } else {
          navigate(`/maptool/map/${mapId}`);
        }
      } else {
        setMessage({
          text: "Poi title cannot be blank",
          background: color.inlineError,
          color: color.inlineErrorText
        });
      }
    }
  };

  if (!formValues)
    return (
      <div>
        <InlineMessage message={{ text: "POI Not Found" }} />
      </div>
    );

  return (
    <>
      {message?.text && <InlineMessage message={message} />}
      <Form
        mapId={mapId}
        formValues={formValues}
        onChange={onChange}
        onSubmit={onSubmit}
        cta={
          <>
            <Button
              onClick={() => navigate(`/maptool/map/${mapId}`)}
              label={"Cancel"}
              className={classes.smallButton}
            />
            <Button label={"Update"} className={classes.smallButton} />
          </>
        }
      />
    </>
  );
};

export default Edit;
