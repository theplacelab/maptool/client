import React from "react";
import { useParams } from "react-router-dom";
import Edit from "./Edit";
import { validatedId } from "src/utils.js";

const AssetAddEdit = () => {
  const { id } = useParams();
  return <Edit id={id} cancelTo={`/maptool/assets`} title="MAP :: Asset" />;
};
export default AssetAddEdit;
