import React from "react";
import { Input, FIELD_TYPE } from "src/components/Form";
import { default as useStyles } from "src/styles.jsx";
import {
  useGetMapByIdQuery,
  useGetPoiFieldQuery,
  useGetPoiDataFormatQuery
} from "src/services/map";

const Form = ({ cta, formValues, mapId, onSubmit, onChange }) => {
  const { data: map } = useGetMapByIdQuery(mapId);
  const { data: poiFields } = useGetPoiFieldQuery(mapId);
  const { data: poiFieldFormat } = useGetPoiDataFormatQuery();
  const classes = useStyles();
  return (
    <form onSubmit={onSubmit} method="POST">
      <div className={classes.panel}>
        <div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <div>
              <Input
                published={true}
                disabled={true}
                onChange={onChange}
                label="Map Name"
                type={FIELD_TYPE.TEXT}
                placeholder={map?.title}
              />
            </div>
            <div>
              <Input
                published={true}
                id="title"
                type={FIELD_TYPE.TEXT}
                onChange={onChange}
                label="POI Name"
                value={formValues.title}
                placeholder="Name of this POI (required)"
              />
            </div>
            <div>
              <Input
                published={true}
                id="coordinates"
                type={FIELD_TYPE.COORDINATES}
                onChange={onChange}
                label="Coordinates (lat, lng)"
                value={formValues.coordinates}
              />
            </div>
            <div className={classes.upperRightCta}>{cta}</div>
            <div className={classes.inputLabel}>Fields</div>
            <div
              style={{ marginBottom: "2rem" }}
              className={classes.inputCustomBorder}
            >
              {poiFields &&
                poiFieldFormat &&
                poiFields?.map((field) => {
                  const { slug } = poiFieldFormat?.find(
                    (format) => format.id === Number(field.poi_data_format_id)
                  );
                  return (
                    <div key={field.id}>
                      <Input
                        editDefaults={true}
                        value={formValues[field.id]}
                        locked={
                          typeof formValues[`${field.id}_locked`] !==
                          "undefined"
                            ? formValues[`${field.id}_locked`]
                            : field.is_locked_default
                        }
                        markdown={
                          typeof formValues[`${field.id}_markdown`] !==
                          "undefined"
                            ? formValues[`${field.id}_markdown`]
                            : field.is_markdown_default
                        }
                        published={
                          typeof formValues[`${field.id}_published`] !==
                          "undefined"
                            ? formValues[`${field.id}_published`]
                            : field.is_published_default
                        }
                        id={field.id}
                        type={FIELD_TYPE[slug]}
                        label={field.label}
                        onChange={onChange}
                      />
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};
export default Form;
