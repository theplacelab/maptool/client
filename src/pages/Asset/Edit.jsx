import React, { useEffect, useState } from "react";
import { color } from "src/styles.jsx";
import { useNavigate } from "react-router-dom";
import {
  useUpdateAssetMutation,
  useGetAssetByIdQuery
} from "src/services/asset";
import { InlineMessage, Button } from "src/components";
import Form from "./Form";
import { default as useStyles } from "src/styles.jsx";

const Edit = ({ id, mapId }) => {
  const navigate = useNavigate();
  const [updateAsset] = useUpdateAssetMutation();
  const { data: assetData } = useGetAssetByIdQuery(id);
  const [message, setMessage] = useState();
  const [formValues, setFormValues] = useState();
  const classes = useStyles();

  useEffect(() => {
    debugger;
    let values = {};
    const pd = assetData ? assetData[0] : null;
    if (pd) {
      for (const key in pd) {
        if (key !== "fields") values[key] = pd[key];
      }
      pd["fields"].forEach((field) => {
        const id = field.asset_data_options_id;
        values[id] = field.value;
        values[`${id}_markdown`] = field.is_markdown;
        values[`${id}_published`] = field.is_published;
        values[`${id}_locked`] = field.is_locked;
      });
      setFormValues(values);
    }
  }, [assetData]);

  const onChange = (e) => {
    setFormValues({
      ...formValues,
      [Number(e.target.id) ? Number(e.target.id) : e.target.id]: e.target.value
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();
    if (Object.keys(formValues).length > 0) {
      if (formValues.title && formValues.title.trim().length > 0) {
        const { error } = await updateAsset({
          mapId,
          assetData: formValues
        });
        if (error) {
          setMessage({
            text: JSON.stringify(error),
            background: color.inlineError,
            color: color.inlineErrorText
          });
        } else {
          navigate(`/maptool/map/${mapId}`);
        }
      } else {
        setMessage({
          text: "Asset title cannot be blank",
          background: color.inlineError,
          color: color.inlineErrorText
        });
      }
    }
  };

  if (!formValues)
    return (
      <div>
        <InlineMessage message={{ text: `Asset ${id} Not Found` }} />
      </div>
    );

  return (
    <>
      {message?.text && <InlineMessage message={message} />}
      <Form
        mapId={mapId}
        formValues={formValues}
        onChange={onChange}
        onSubmit={onSubmit}
        cta={
          <>
            <Button
              onClick={() => navigate(`/maptool/map/${mapId}`)}
              label={"Cancel"}
              className={classes.smallButton}
            />
            <Button label={"Update"} className={classes.smallButton} />
          </>
        }
      />
    </>
  );
};

export default Edit;
