import React, { useState, useEffect } from "react";
import { default as useStyles } from "src/styles.jsx";
import { useGetMapsQuery } from "src/services/map";
import { useNavigate } from "react-router-dom";
import { SortableList } from "src/components";
import { Button, Collapsable } from "src/components";
import {
  useUpdateMapOrderMutation,
  useDeleteMapMutation
} from "src/services/map";
import MapGlobalSettings from "./MapGlobalSettings";
import { DndContext, closestCenter } from "@dnd-kit/core";
import { restrictToVerticalAxis } from "@dnd-kit/modifiers";
import {
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors
} from "@dnd-kit/core";
import { sortableKeyboardCoordinates } from "@dnd-kit/sortable";
import { arrayMove } from "@dnd-kit/sortable";

const Maps = () => {
  const navigate = useNavigate();
  const { data } = useGetMapsQuery();

  const classes = useStyles();
  const [updateMapOrder] = useUpdateMapOrderMutation();
  const [deleteMap] = useDeleteMapMutation();

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates
    })
  );

  const [mapList, setMapList] = useState();
  useEffect(() => {
    setMapList(data);
  }, [data]);

  const handleDragEnd = (e) => {
    const { active, over } = e;
    if (active.id !== over.id) {
      setMapList((items) => {
        const oldIndex = items.indexOf(items.find((i) => i.id === active.id));
        const newIndex = items.indexOf(items.find((i) => i.id === over.id));
        const reorderedItems = arrayMove(items, oldIndex, newIndex);
        updateMapOrder(reorderedItems.map((i) => i.id));
        return reorderedItems;
      });
    }
  };

  return (
    <div className={classes.adminContent}>
      <div className={classes.title}>Maps</div>
      <div className={classes.panel}>
        <div className={classes.inputCustomBorder}>
          <Collapsable
            isOpen={false}
            title="Global Options (apply to all maps)"
          >
            <MapGlobalSettings />
          </Collapsable>
        </div>
        <div style={{ height: "2rem" }}></div>
        <div>
          <DndContext
            modifiers={[restrictToVerticalAxis]}
            sensors={sensors}
            collisionDetection={closestCenter}
            onDragEnd={handleDragEnd}
          >
            <SortableList
              data={mapList?.map((item) => ({
                id: item.id,
                label: item.title
              }))}
              itemActions={
                <>
                  <Button
                    onClick={(e) => {
                      const id = Number(
                        e.target.parentElement.parentElement.dataset.id
                      );
                      if (
                        window.confirm(
                          "Are you sure you want to delete this map?"
                        )
                      ) {
                        deleteMap(id);
                      }
                    }}
                    style={{ minWidth: "5rem", maxWidth: "5rem" }}
                    label={"Delete"}
                    className={classes.smallButton}
                  />
                  <Button
                    onClick={(e) => {
                      const id = Number(
                        e.target.parentElement.parentElement.dataset.id
                      );
                      navigate(`/maptool/map/${id}`);
                    }}
                    style={{ minWidth: "5rem", maxWidth: "5rem" }}
                    label={"Edit"}
                    className={classes.smallButton}
                  />
                </>
              }
            />
            <div className={classes.upperRightCta}>
              <Button
                onClick={() => navigate(`/maptool/map/new`)}
                label={"Add"}
                className={classes.smallButton}
              />
            </div>
          </DndContext>
        </div>
      </div>
    </div>
  );
};
export default Maps;
