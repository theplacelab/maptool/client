import React, { useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { default as useStyles } from "src/styles.jsx";
import { Dialog } from "src/components";
import { validatedId } from "src/utils.js";

const AddEdit = ({ cancelTo, title, Add, Edit, id, mapId }) => {
  const { id: param_id } = useParams();
  const useId = id ? validatedId(id) : validatedId(param_id);
  const navigate = useNavigate();
  const classes = useStyles();
  const [dialogProps, setDialogProps] = useState({
    isVisible: false
  });

  return (
    <>
      <div className={classes.adminContent}>
        <Dialog {...dialogProps} />
        <div className={classes.title}>
          <span onClick={() => navigate(cancelTo)}>{title}</span> ::{" "}
          {useId == null ? "Add" : "Edit"}
        </div>
        {useId == null ? (
          <Add id={useId} mapId={mapId} />
        ) : (
          <Edit id={useId} mapId={mapId} setDialog={setDialogProps} />
        )}
      </div>
    </>
  );
};

export default AddEdit;
