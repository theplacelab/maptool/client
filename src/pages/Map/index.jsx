import React from "react";
import { AddEdit } from "../Common/";
import Add from "./Add";
import Edit from "./Edit";

const MapAddEdit = () => {
  return <AddEdit cancelTo={`../maps`} title="Map" Add={Add} Edit={Edit} />;
};

export default MapAddEdit;
