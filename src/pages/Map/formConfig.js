import { FIELD_TYPE } from "src/components/Form";
const config = {
  version: {
    displayText: "*",
    type: FIELD_TYPE.NONE
  },
  backgroundColor: {
    displayText: "Map Background Color",
    type: FIELD_TYPE.COLOR
  },
  hasTitle: {
    displayText: "Display Title",
    type: FIELD_TYPE.CHECKBOX
  },
  hasNavigation: {
    displayText: "Display Navigation Widget",
    type: FIELD_TYPE.CHECKBOX
  },
  hasInfo: {
    displayText: "Display Coordinate Info",
    type: FIELD_TYPE.CHECKBOX
  },
  zoomInitial: {
    displayText: "Initial Zoom",
    type: FIELD_TYPE.NUMBER
  },
  zoomMin: {
    displayText: "Zoom Extent Minimum",
    type: FIELD_TYPE.NUMBER
  },
  zoomMax: {
    displayText: "Zoom Extent Maximum",
    type: FIELD_TYPE.NUMBER
  },
  centerInitial: {
    displayText: "Initial Center",
    type: FIELD_TYPE.TEXT
  }
};
export default config;
