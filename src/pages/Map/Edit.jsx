import React, { useState, useEffect } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Input, FIELD_TYPE } from "src/components/Form";
import {
  Button,
  InlineMessage,
  Collapsable,
  SortableList,
  Spinner
} from "src/components";
import { MapStyleEditor } from "src/components/Map";
import { color, icon } from "src/styles.jsx";
import {
  useUpdateMapMutation,
  useGetMapByIdQuery,
  useGetPoiFieldQuery,
  useGetPoiDataFormatQuery,
  useDeletePoiFieldMutation,
  useUpdatePoiFieldOrderMutation,
  useUpdatePoiOrderMutation,
  useDeletePoiMutation
} from "src/services/map";
import formConfig from "./formConfig.js";
import { useNavigate } from "react-router-dom";
import { useGetPoiQuery } from "src/services/map";
import { DndContext, closestCenter } from "@dnd-kit/core";
import { restrictToVerticalAxis } from "@dnd-kit/modifiers";
import {
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors
} from "@dnd-kit/core";
import { sortableKeyboardCoordinates, arrayMove } from "@dnd-kit/sortable";
import { importPoiFromFile } from "src/import";
import Import from "src/pages/Poi/Import";

const Edit = ({ id, setDialog }) => {
  const navigate = useNavigate();
  const { data: poiDataFormat } = useGetPoiDataFormatQuery();
  const { data: map } = useGetMapByIdQuery(id);
  const { data: poiListData } = useGetPoiQuery(id);
  const { data: poiFields } = useGetPoiFieldQuery(id);
  const classes = useStyles();
  const [message, setMessage] = useState({ text: "" });
  const [hasUnsavedChanges, setHasUnsavedChanges] = useState(false);
  const [poiFieldList, setPoiFieldList] = useState();
  const [poiList, setPoiList] = useState();
  const [updateMap] = useUpdateMapMutation();
  const [deletePoiField] = useDeletePoiFieldMutation();
  const [deletePoi] = useDeletePoiMutation();
  const [updatePoiFieldOrder] = useUpdatePoiFieldOrderMutation();
  const [updatePoiOrder] = useUpdatePoiOrderMutation();
  const [importData, setImportData] = useState();
  const [isImporting, setIsImporting] = useState(false);

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates
    })
  );

  const [formValues, _setFormValues] = useState({
    id: !isNaN(parseInt(id, 10)) ? parseInt(id, 10) : null
  });

  const setFormValues = (newFormValues) => {
    setHasUnsavedChanges(true);
    _setFormValues(newFormValues);
  };

  useEffect(() => {
    if (!isNaN(parseInt(id, 10))) {
      setFormValues((formValues) => {
        return {
          ...formValues,
          ...map
        };
      });
      setHasUnsavedChanges(false);
    }
  }, [map, id]);

  useEffect(() => {
    setPoiFieldList(
      poiFields?.map((item) => {
        const fieldType = poiDataFormat
          ?.find((format) => format.id === item.poi_data_format_id)
          .display_name.replaceAll(" ", "")
          .replaceAll("/", "")
          .toUpperCase();

        const searchable = item.is_searchable
          ? icon.FIELD_STATUS.SEARCHABLE
          : null;
        const markdown = item.is_markdown_default
          ? icon.FIELD_STATUS.MARKDOWN
          : null;
        const published = item.is_published_default
          ? icon.FIELD_STATUS.PUBLISHED
          : icon.FIELD_STATUS.UNPUBLISHED;
        const locked = item.is_locked_default ? icon.FIELD_STATUS.LOCKED : null;

        return {
          ...item,
          label: (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <div style={{ color: color.backgroundTint2 }}>
                {icon.FIELD_TYPE[fieldType]}
              </div>
              <div style={{ minWidth: "3rem", marginLeft: "1rem" }}>
                {item.label}
              </div>
              <div
                style={{
                  flexGrow: 1,
                  justifyContent: "flex-end",
                  marginLeft: "1rem",
                  color: color.accent
                }}
              >
                {searchable} {markdown} {published}
                {locked}
              </div>
            </div>
          )
        };
      })
    );
  }, [setPoiFieldList, poiDataFormat, poiFields]);

  useEffect(() => {
    setPoiList(
      poiListData?.map((item) => ({
        id: item.id,
        label: item.title
      }))
    );
  }, [poiListData, setPoiList]);

  const onSubmit = async (e) => {
    e.preventDefault();
    e.target.reset();
    if (Object.keys(formValues).length > 1) {
      Object.keys(formValues).forEach((key) => {
        if (typeof formValues[key] === "string")
          formValues[key] = formValues[key].trim();
      });
      if (map) {
        // Edit
        const { error } = await updateMap(formValues);
        if (error) {
          console.log(error);
          setMessage({ text: error.error });
          setFormValues({ ...formValues, title: map.title });
        } else {
          setMessage({
            text: "Changes saved!",
            background: color.inlineOk,
            color: color.inlineOkText
          });
        }
      }
    }
  };

  const onChange = (e) => {
    let newFormValues = { ...formValues };
    if (Object.keys(formValues.settings).includes(e.target.id)) {
      newFormValues = {
        ...newFormValues,
        settings: {
          ...newFormValues.settings,
          [e.target.id]: e.target.value
        }
      };
    } else {
      newFormValues = {
        ...newFormValues,
        [e.target.id]: e.target.value
      };
    }

    setFormValues(newFormValues);
  };

  if (typeof id === "undefined") return null;
  const fv = { ...formValues, settings: { ...formValues.settings } };

  if (importData) {
    if (isImporting) setIsImporting(false);
    return <Import mapId={id} data={importData} />;
  }

  return (
    <React.Fragment>
      {isImporting && <Spinner />}
      <form onSubmit={onSubmit} method="POST">
        <div className={classes.panel}>
          <div>
            <div style={{ display: "flex", flexDirection: "column" }}>
              {message.text && <InlineMessage message={message} />}

              <div>
                <Input
                  onChange={onChange}
                  label="Title"
                  id="title"
                  type={FIELD_TYPE.TEXT}
                  autoComplete="title"
                  value={formValues.title}
                  placeholder="Title of this map (required)"
                />
              </div>
              <div>
                <Input
                  onChange={onChange}
                  label="Slug"
                  id="slug"
                  type={FIELD_TYPE.TEXT}
                  autoComplete="slug"
                  value={formValues.slug}
                  placeholder="Leave blank to auto-generate"
                />
              </div>

              <div className={classes.inputLabel}>Tiles</div>

              <div className={classes.inputCustomBorder}>
                <Collapsable isOpen={false} title="Map Style">
                  <MapStyleEditor
                    onChange={onChange}
                    formValues={formValues}
                    setFormValues={setFormValues}
                    setMessage={setMessage}
                    mapConfig={{
                      title: formValues.title,
                      style: fv.style_json,
                      initialZoom: fv.settings?.zoom,
                      hasTitle: fv.settings?.hasTitle,
                      hasInfo: fv.settings?.hasInfo,
                      hasNavigation: fv.settings?.hasNavigation,
                      backgroundColor: fv.settings?.backgroundColor
                    }}
                  />
                </Collapsable>
              </div>

              <div className={classes.inputLabel}>Options</div>
              <div className={classes.inputCustomBorder}>
                <Collapsable
                  isOpen={false}
                  title="Map Options (apply only to this map)"
                >
                  <div>
                    {formValues.settings &&
                      Object.keys(formValues?.settings)?.map((fieldName) => {
                        return (
                          <div key={fieldName}>
                            <Input
                              id={fieldName}
                              onChange={onChange}
                              type={
                                formConfig[fieldName].displayText === "*"
                                  ? FIELD_TYPE.HIDDEN
                                  : formConfig[fieldName].type
                              }
                              label={formConfig[fieldName].displayText}
                              value={formValues.settings[fieldName]}
                            />
                          </div>
                        );
                      })}
                  </div>
                </Collapsable>
              </div>

              <div className={classes.inputLabel}>Points of Interest</div>
              <div className={classes.inputCustomBorder}>
                <Collapsable isOpen={true} title="Points of Interest">
                  <div className={classes.inputLabel}>Fields</div>
                  <div
                    style={{ marginBottom: "2rem" }}
                    className={classes.inputCustomBorder}
                  >
                    <Collapsable
                      isOpen={false}
                      title="Fields (for all POI on this map)"
                    >
                      <DndContext
                        modifiers={[restrictToVerticalAxis]}
                        sensors={sensors}
                        collisionDetection={closestCenter}
                        onDragEnd={(e) => {
                          const { active, over } = e;
                          if (active.id !== over.id) {
                            setPoiFieldList((items) => {
                              const oldIndex = items.indexOf(
                                items.find((i) => i.id === active.id)
                              );
                              const newIndex = items.indexOf(
                                items.find((i) => i.id === over.id)
                              );
                              const reorderedItems = arrayMove(
                                items,
                                oldIndex,
                                newIndex
                              );
                              updatePoiFieldOrder({
                                mapId: id,
                                order: reorderedItems.map((i) => i.id)
                              });
                              return reorderedItems;
                            });
                          }
                        }}
                      >
                        <SortableList
                          data={poiFieldList}
                          collectionActions={
                            <Button
                              onClick={() =>
                                navigate(`/maptool/map/${id}/poi/config/new`)
                              }
                              label={"Add"}
                              className={classes.smallButton}
                            />
                          }
                          itemActions={
                            <>
                              <Button
                                label={"Delete"}
                                className={classes.smallButton}
                                style={{ minWidth: "5rem", maxWidth: "5rem" }}
                                onClick={(e) => {
                                  const configId = Number(
                                    e.target.parentElement.parentElement.dataset
                                      .id
                                  );
                                  if (
                                    window.confirm(
                                      "Are you sure you want to delete this field?"
                                    )
                                  ) {
                                    deletePoiField(configId);
                                  }
                                }}
                              />
                              <Button
                                label={"Edit"}
                                className={classes.smallButton}
                                style={{ minWidth: "5rem", maxWidth: "5rem" }}
                                onClick={(e) => {
                                  const configId = Number(
                                    e.target.parentElement.parentElement.dataset
                                      .id
                                  );
                                  navigate(
                                    `/maptool/map/${id}/poi/config/${configId}`
                                  );
                                }}
                              />
                            </>
                          }
                        />
                      </DndContext>
                    </Collapsable>
                  </div>

                  <DndContext
                    modifiers={[restrictToVerticalAxis]}
                    sensors={sensors}
                    collisionDetection={closestCenter}
                    onDragEnd={(e) => {
                      const { active, over } = e;
                      if (active.id !== over.id) {
                        setPoiList((items) => {
                          const oldIndex = items.indexOf(
                            items.find((i) => i.id === active.id)
                          );
                          const newIndex = items.indexOf(
                            items.find((i) => i.id === over.id)
                          );
                          const reorderedItems = arrayMove(
                            items,
                            oldIndex,
                            newIndex
                          );
                          updatePoiOrder({
                            mapId: id,
                            order: reorderedItems.map((i) => i.id)
                          });
                          return reorderedItems;
                        });
                      }
                    }}
                  >
                    <SortableList
                      data={poiList}
                      collectionActions={
                        <div style={{ display: "flex", flexDirection: "row" }}>
                          <Button
                            onClick={() => {
                              setIsImporting(true);
                              importPoiFromFile((data) => {
                                if (!data) {
                                  setIsImporting(false);
                                  window.alert("Unrecognized format!");
                                } else if (data.error) {
                                  setIsImporting(false);
                                  return;
                                }
                                setImportData(data);
                              });
                            }}
                            label={"Import"}
                            className={classes.smallButton}
                          />
                          <Button
                            onClick={() =>
                              navigate(`/maptool/map/${id}/poi/new`)
                            }
                            label={"Add"}
                            className={classes.smallButton}
                          />{" "}
                        </div>
                      }
                      itemActions={
                        <>
                          <Button
                            label={"Delete"}
                            className={classes.smallButton}
                            style={{ minWidth: "5rem", maxWidth: "5rem" }}
                            onClick={(e) => {
                              const poiId = Number(
                                e.target.parentElement.parentElement.dataset.id
                              );
                              if (
                                window.confirm(
                                  "Are you sure you want to delete this poi ?"
                                )
                              ) {
                                deletePoi({ mapId: id, poiId });
                              }
                            }}
                          />
                          <Button
                            label={"Edit"}
                            className={classes.smallButton}
                            style={{ minWidth: "5rem", maxWidth: "5rem" }}
                            onClick={(e) => {
                              const poiId = Number(
                                e.target.parentElement.parentElement.dataset.id
                              );
                              navigate(`/maptool/map/${id}/poi/${poiId}`);
                            }}
                          />
                        </>
                      }
                    />
                  </DndContext>
                  {/*
                  <Table
                    onSaveSortOrder={(orderedList) => {
                      const newOrder = orderedList?.map((item) => item.id);
                      //updateOrder(newOrder);
                    }}
                    show={["Sort", "title"]}
                    data={poiList}
                    onClick={(poiId) =>
                      navigate(`/maptool/map/${id}/poi/${poiId}`)
                    }
                    cta={
                      <div>
                        <Button
                          onClick={() => navigate(`/maptool/map/${id}/poi/new`)}
                          label={"Add"}
                          className={classes.smallButton}
                        />
                      </div>
                    }
                  /> 
                  */}
                </Collapsable>
              </div>

              <div className={classes.upperRightCta}>
                <Button
                  label={"Cancel"}
                  type="button"
                  className={classes.smallButton}
                  onClick={() => navigate(`/maptool/maps`)}
                />
                <Button
                  label={"Save"}
                  disabled={!hasUnsavedChanges}
                  className={classes.smallButton}
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    </React.Fragment>
  );
};

export default Edit;
