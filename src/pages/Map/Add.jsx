import React, { useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Input } from "src/components/Form";
import { Button, InlineMessage } from "src/components";
import { color } from "src/styles.jsx";
import { useNavigate } from "react-router-dom";
import { useCreateMapMutation } from "src/services/map";

const Add = () => {
  const navigate = useNavigate();
  const classes = useStyles();
  const [message, setMessage] = useState({ text: "" });
  const [createMap] = useCreateMapMutation();
  const [formValues, setFormValues] = useState({});

  const onChange = (e) => {
    setFormValues({ ...formValues, [e.target.id]: e.target.value?.trim() });
  };

  return (
    <form
      onSubmit={async (e) => {
        e.preventDefault();
        e.target.reset();
        if (Object.keys(formValues).length > 0) {
          if (formValues.title && formValues.title.trim().length > 0) {
            const { data, error } = await createMap(formValues);
            if (error) {
              setMessage({
                text: JSON.stringify(error),
                background: color.inlineError,
                color: color.inlineErrorText
              });
            } else {
              navigate(`/maptool/map/${data.id}`);
            }
          } else {
            setMessage({
              text: "Map title cannot be blank",
              background: color.inlineError,
              color: color.inlineErrorText
            });
          }
        }
      }}
      method="POST"
    >
      <div className={classes.panel}>
        <div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {message.text && <InlineMessage message={message} />}
            <div>
              <Input
                id="title"
                type="text"
                autoComplete="title"
                onChange={onChange}
                label="Title"
                value={formValues.title}
                placeholder="Title of this map (required)"
              />
            </div>
            <div>
              <Input
                id="slug"
                type="text"
                autoComplete="slug"
                onChange={onChange}
                label="Slug"
                value={formValues.slug}
                placeholder="Leave blank to auto-generate"
              />
            </div>
            <div className={classes.upperRightCta}>
              <Button label={"Add"} className={classes.smallButton} />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Add;
