import React from "react";
import { default as useStyles } from "src/styles.jsx";
import { useGetUsersQuery } from "src/services/auth";
import { useNavigate } from "react-router-dom";
import { Table } from "src/components";
import { Button } from "src/components";

const Users = () => {
  const navigate = useNavigate();
  const { data: userList } = useGetUsersQuery();
  const classes = useStyles();

  return (
    <div className={classes.adminContent}>
      <div className={classes.title}>Users</div>
      <div className={classes.panel}>
        <div>
          <Table
            show={["name", "email"]}
            data={userList?.data}
            onClick={(uuid) => navigate(`/maptool/user/${uuid}`)}
          />
          <div className={classes.upperRightCta}>
            <Button
              onClick={() => navigate(`/maptool/user/new`)}
              label={"Add"}
              className={classes.smallButton}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Users;
