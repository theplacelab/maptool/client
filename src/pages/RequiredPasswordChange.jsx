import React from "react";
import { default as useStyles } from "src/styles.jsx";
import { Button } from "src/components";
import UserPasswordChange from "src/pages/UserPasswordChange";
import { useDispatch } from "react-redux";
import { clearCredentials } from "src/redux/slice/auth";

export default function RequiredPasswordChange({ user }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  return (
    <div className={classes.container}>
      <div
        style={{
          margin: "auto",
          padding: "1rem",
          width: "30rem",
          maxWidth: "40rem",
          minWidth: "300px"
        }}
      >
        <div className={classes.title} style={{ textAlign: "center" }}>
          Change your Password
        </div>
        <div style={{ textAlign: "center" }}>
          Please change your password to continue
        </div>
        <UserPasswordChange user={user} forcedChange={true} />
        <div style={{ display: "flex", width: "26rem", margin: "auto" }}>
          <Button onClick={() => dispatch(clearCredentials())} label="Cancel" />
        </div>
      </div>
    </div>
  );
}
