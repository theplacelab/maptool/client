import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { validate } from "src/services/auth";
import jwt_decode from "jwt-decode";
import { default as useStyles } from "src/styles.jsx";
import { Button } from "src/components";

export default function Validate() {
  const classes = useStyles();
  const [validationStatus, setValidationStatus] = useState();

  const params = useParams();
  const navigate = useNavigate();
  localStorage.clear();
  useEffect(() => {
    async function checkValidation() {
      const result = await validate(params.validationKey);
      let claims = {};
      try {
        claims = jwt_decode(params.validationKey);
      } catch {}
      setValidationStatus({ result, claims });
    }
    checkValidation();
  }, [params]);

  if (validationStatus?.result.code === 410) {
    return (
      <div className={classes.container}>
        {" "}
        <div
          style={{
            margin: "auto",
            padding: "1rem",
            width: "30rem",
            maxWidth: "40rem",
            minWidth: "300px"
          }}
        >
          <div className={classes.title} style={{ textAlign: "center" }}>
            Validation Link Expired
            <div style={{ display: "flex", flexDirection: "row" }}>
              <Button
                style={{ margin: ".5rem" }}
                label="Ok"
                onClick={() => navigate(`/maptool/`)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
  if (validationStatus?.result.code === 200)
    navigate(`/maptool/${validationStatus?.claims.email}`);

  return <div>Validating...</div>;
}
