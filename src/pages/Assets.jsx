import React from "react";
import * as tus from "tus-js-client";
import { default as useStyles } from "src/styles.jsx";
import { setAlert } from "src/redux/slice/alert";
import { useDispatch, useSelector } from "react-redux";
import { color } from "src/styles";
import { assetApi, useGetAssetsQuery } from "../services/asset";
import { Button, Table } from "src/components";
import { useNavigate } from "react-router-dom";

const selectFile = (contentType, multiple, onChange) => {
  return new Promise((resolve) => {
    let input = document.createElement("input");
    input.type = "file";
    input.multiple = multiple;
    input.accept = contentType;
    input.onchange = onChange;
    input.click();
  });
};

export default function Assets() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { data: assetList } = useGetAssetsQuery();

  const classes = useStyles();
  const authToken = useSelector((state) => state.auth.token.auth);
  const [percentComplete, setPercentComplete] = React.useState(0);
  const [uploadInfo, setUploadInfo] = React.useState();

  const onUpload = (e) => {
    const file = e.target.files[0];
    const upload = new tus.Upload(file, {
      headers: {
        Authorization: `Bearer ${authToken}`
      },
      endpoint: `${_env_.VITE_ASSET_SERVICE}/files/`,
      removeFingerprintOnSuccess: true,
      metadata: {
        filename: file.name,
        filetype: file.type
      },
      retryDelays: [0, 1000, 3000, 5000], // the delays for retrying the upload in case of an error
      onError: (errMsg) => {
        console.error(errMsg);
        dispatch(
          setAlert({
            text: errMsg.message,
            color: color.error,
            expiresAfter: 5000
          })
        );
      },
      onProgress: (bytesUploaded, bytesTotal) =>
        setPercentComplete(((bytesUploaded / bytesTotal) * 100).toFixed(2)),
      onSuccess: () => {
        dispatch(assetApi.util.invalidateTags(["Assets"]));

        setPercentComplete(0);
        console.log(upload);
        setUploadInfo({
          file: upload.file,
          options: upload.options,
          url: upload.url,
          metadata: upload.metadata
        });
      }
    });
    upload.findPreviousUploads().then((previousUploads) => {
      if (previousUploads.length) {
        upload.resumeFromPreviousUpload(previousUploads[0]);
      } else {
        upload.start();
      }
    });
  };

  return (
    <div className={classes.adminContent}>
      <div className={classes.title}>Assets</div>
      <div
        style={{
          border: "1px solid black",
          height: "2rem",
          opacity: `${Number(percentComplete) > 0 ? 1 : 0}`
        }}
      >
        <div
          style={{
            background: "orange",
            width: `${
              Number(percentComplete) + Number(percentComplete) > 0 ? 10 : 0
            }%`,
            maxWidth: `${
              Number(percentComplete) + Number(percentComplete) > 0 ? 10 : 0
            }%`
          }}
        >
          &nbsp;
        </div>
      </div>
      <input
        style={{ opacity: `${Number(percentComplete) > 0 ? 0 : 1}` }}
        disabled={percentComplete > 0}
        type="file"
        onChange={onUpload}
      />

      <div>
        <Table
          show={["uuid", "display_name"]}
          data={assetList}
          onClick={(id) => navigate(`/maptool/asset/${id}`)}
          actions={
            <div>
              <Button
                onClick={() => selectFile("*/*", true, onUpload)}
                label={"Add"}
                className={classes.smallButton}
              />
            </div>
          }
        />
      </div>
    </div>
  );
}
