import React, { useState, useEffect } from "react";
import { default as useStyles } from "src/styles.jsx";
import formConfig from "./formConfig.js";
import { FIELD_TYPE, Input } from "src/components/Form";
import { Button } from "src/components";
import {
  useGetGlobalSettingsQuery,
  useUpdateGlobalSettingsMutation
} from "src/services/map";

const MapGlobalSettings = () => {
  const { data: globalSettings } = useGetGlobalSettingsQuery();
  const mapSettings = globalSettings?.map;
  const classes = useStyles();
  const [hasUnsavedChanges, setHasUnsavedChanges] = useState(false);
  const [formValues, _setFormValues] = useState();
  const setFormValues = (newFormValues) => {
    setHasUnsavedChanges(true);
    _setFormValues(newFormValues);
  };

  const onChange = (e) => {
    setFormValues({ ...formValues, [e.target.id]: e.target.value });
  };

  const [updateSettings] = useUpdateGlobalSettingsMutation();
  const onSubmit = (e) => {
    updateSettings({ map: { ...formValues } });
  };

  useEffect(() => {
    setFormValues((formValues) => {
      return {
        ...formValues,
        ...mapSettings
      };
    });
    setHasUnsavedChanges(false);
  }, [mapSettings]);

  return (
    <React.Fragment>
      <div
        style={{ display: "flex", flexDirection: "row", position: "relative" }}
      >
        <div style={{ flexGrow: 1 }} />
        <div style={{ position: "absolute", top: "-4rem", right: "-2rem" }}>
          <Button
            disabled={!hasUnsavedChanges}
            onClick={onSubmit}
            label={"Save Options"}
            className={classes.smallButton}
          />
        </div>
      </div>

      {formValues &&
        Object.keys(formValues)?.map((fieldName) =>
          formConfig[fieldName] ? (
            <div key={fieldName}>
              <Input
                id={fieldName}
                onChange={onChange}
                type={
                  formConfig[fieldName].displayText === "*"
                    ? FIELD_TYPE.HIDDEN
                    : formConfig[fieldName].type
                }
                label={formConfig[fieldName].displayText}
                value={formValues[fieldName]}
              />
            </div>
          ) : null
        )}
    </React.Fragment>
  );
};
export default MapGlobalSettings;
