import { FIELD_TYPE } from "src/components/Form";
const config = {
  version: {
    displayText: "*",
    type: FIELD_TYPE.NONE
  },
  hasMapSelector: {
    displayText: "Map Selector",
    type: FIELD_TYPE.CHECKBOX
  },
  hasPagination: {
    displayText: "Pagination",
    type: FIELD_TYPE.CHECKBOX
  },
  hasGeoSearch: {
    displayText: "*", //"Geo Search",
    type: FIELD_TYPE.CHECKBOX
  },
  hasTimeFilter: {
    displayText: "*", //"Time Filter",
    type: FIELD_TYPE.CHECKBOX
  },
  hasAbout: {
    displayText: "*", //"About",
    type: FIELD_TYPE.CHECKBOX
  },
  hasPoiSearch: {
    displayText: "*", //"POI Search",
    type: FIELD_TYPE.CHECKBOX
  },
  hasFitPoi: {
    displayText: "*", //"Fit Poi",
    type: FIELD_TYPE.CHECKBOX
  },
  hasUserLocationWidget: {
    displayText: "*", //"User Location",
    type: FIELD_TYPE.CHECKBOX
  }
};
export default config;
