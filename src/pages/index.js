import AdminMenu from "src/pages/AdminMenu";
import Asset from "src/pages/Asset";
import Assets from "src/pages/Assets";
import Login from "src/pages/Login";
import Maps from "src/pages/Maps";
import Poi from "src/pages/Poi";
import PoiField from "src/pages/PoiField";
import Settings from "src/pages/Settings";
import Users from "src/pages/Users";
import Validate from "src/pages/Validate";
import User from "src/pages/User";
import Map from "src/pages/Map";
import Error from "src/pages/Error";
import RequiredPasswordChange from "src/pages/RequiredPasswordChange";
import RequiredValidation from "src/pages/RequiredValidation";

export {
  AdminMenu,
  Asset,
  Assets,
  Login,
  Maps,
  Settings,
  Users,
  Validate,
  User,
  Map,
  Poi,
  PoiField,
  Error,
  RequiredPasswordChange,
  RequiredValidation
};
