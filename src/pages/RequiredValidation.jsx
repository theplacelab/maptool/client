import React, { useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Button } from "src/components";
import { setAlert } from "src/redux/slice/alert";
import { useDispatch } from "react-redux";
import { clearCredentials } from "src/redux/slice/auth";
import { resetUser } from "src/services/auth";

export default function RequiredValidation({ user }) {
  const classes = useStyles();
  const [resetSent, setResetSent] = useState(false);
  const dispatch = useDispatch();

  const forgotPassword = async () => {
    const resetStatus = await resetUser(user.email.trim());
    dispatch(setAlert(resetStatus));
    setResetSent(true);
  };

  return (
    <div className={classes.container}>
      <div
        style={{
          margin: "auto",
          padding: "1rem",
          width: "30rem",
          maxWidth: "40rem",
          minWidth: "300px"
        }}
      >
        <div className={classes.title} style={{ textAlign: "center" }}>
          Validate Your Account
        </div>
        <div style={{ textAlign: "center" }}>
          Please click the validation link or enter the validation code we have
          sent to {user.email}
        </div>
        <div
          style={{
            display: "flex",
            width: "26rem",
            margin: "1rem auto 1rem auto"
          }}
        >
          <Button
            disabled={resetSent}
            onClick={forgotPassword}
            label="Re-Send"
          />
        </div>
        <div style={{ display: "flex", width: "26rem", margin: "auto" }}>
          <Button onClick={() => dispatch(clearCredentials())} label="Cancel" />
        </div>
      </div>
    </div>
  );
}
