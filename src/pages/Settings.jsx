import React from "react";
import { color, default as useStyles } from "src/styles.jsx";
import { useSelector } from "react-redux";
import { IndicatorLamp } from "src/components";
import {
  useGetPoiDataFormatQuery,
  useGetPoiStyleQuery
} from "src/services/map";

export default function Settings() {
  const classes = useStyles();
  const serviceStatus = useSelector((state) => state.status.service);
  const { data: poiDataFormat } = useGetPoiDataFormatQuery();
  const { data: poiStyle } = useGetPoiStyleQuery();

  return (
    <div className={classes.adminContent}>
      <div className={classes.title}>Settings</div>
      <div className={classes.panel}>
        <div style={styles.box}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
              justifyContent: "left"
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",

                width: "100%"
              }}
            >
              <IndicatorLamp label={"Auth"} up={serviceStatus.auth.up} />
              Authorization Service: {window._env_.VITE_AUTH_SERVICE}
            </div>{" "}
            <div
              style={{
                display: "flex",
                alignItems: "center",
                width: "100%"
              }}
            >
              <IndicatorLamp label={`Map`} up={serviceStatus.map.up} /> Map
              Service: {window._env_.VITE_MAP_SERVICE}{" "}
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                width: "100%"
              }}
            >
              <IndicatorLamp label={"Asset"} up={serviceStatus.asset.up} />
              Asset Service: {window._env_.VITE_ASSET_SERVICE}
            </div>
          </div>
        </div>
      </div>
      <div className={classes.panel}>
        <div style={styles.box}>
          <div style={{ fontWeight: 900 }}>POI Styles</div>
          <div>
            {poiStyle?.map((style) => (
              <div>{style.display_name}</div>
            ))}
          </div>
        </div>
      </div>
      <div className={classes.panel}>
        <div style={styles.box}>
          <div style={{ fontWeight: 900 }}>POI Data Options</div>
          <div>
            {poiDataFormat?.map((style) => (
              <div>{style.display_name}</div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

const styles = {
  box: {
    borderRadius: "0.3rem",
    padding: "1rem",
    minHeight: "10rem",
    border: `1px solid ${color.backgroundTint1}`
  }
};
