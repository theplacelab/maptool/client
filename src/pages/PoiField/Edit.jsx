import React, { useEffect, useState } from "react";
import { default as useStyles } from "src/styles.jsx";
import { Button, InlineMessage } from "src/components";
import { color, icon } from "src/styles.jsx";
import { useNavigate } from "react-router-dom";
import {
  useUpdatePoiFieldMutation,
  useGetMapByIdQuery,
  useGetPoiFieldByIdQuery,
  useGetPoiDataFormatQuery
} from "src/services/map";
import { Input, FIELD_TYPE } from "src/components/Form";

const Edit = ({ id, mapId }) => {
  const { data: poiDataFormat } = useGetPoiDataFormatQuery();
  const { data: map } = useGetMapByIdQuery(mapId);
  const { data: PoiField } = useGetPoiFieldByIdQuery({ mapId, configId: id });

  const navigate = useNavigate();
  const classes = useStyles();
  const [message, setMessage] = useState({ text: "" });
  const [updatePoiField] = useUpdatePoiFieldMutation();
  const [formValues, setFormValues] = useState();

  useEffect(() => {
    setFormValues(PoiField);
  }, [PoiField]);

  const onChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.id]:
        typeof e.target.value === "string"
          ? e.target.value.trim()
          : e.target.value
    });
  };

  if (!formValues) return null;

  return (
    <form
      onSubmit={async (e) => {
        e.preventDefault();
        e.target.reset();
        if (Object.keys(formValues).length > 0) {
          if (formValues.label && formValues.label.trim().length > 0) {
            formValues.poi_data_format_id = Number(
              formValues.poi_data_format_id
                ? formValues.poi_data_format_id
                : poiDataFormat[0].id
            );
            const { error } = await updatePoiField({
              mapId,
              data: formValues
            });
            if (error) {
              setMessage({
                text: JSON.stringify(error),
                background: color.inlineError,
                color: color.inlineErrorText
              });
            } else {
              navigate(`/maptool/map/${mapId}`);
            }
          } else {
            setMessage({
              text: "Config label cannot be blank",
              background: color.inlineError,
              color: color.inlineErrorText
            });
          }
        }
      }}
      method="POST"
    >
      <div className={classes.panel}>
        <div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {message.text && <InlineMessage message={message} />}
            <div>
              <Input
                disabled={true}
                onChange={onChange}
                label="Map"
                type={FIELD_TYPE.TEXT}
                autoComplete="title"
                placeholder={map?.title}
              />
              <Input
                id="map_id"
                type={FIELD_TYPE.HIDDEN}
                placeholder={map?.id}
              />
            </div>
            <div>
              <Input
                id="label"
                type={FIELD_TYPE.TEXT}
                onChange={onChange}
                label="Label"
                value={formValues.label}
                placeholder="POI Label"
              />
            </div>
            <div>
              <Input
                id="poi_data_format_id"
                type={FIELD_TYPE.SELECT}
                options={poiDataFormat}
                onChange={onChange}
                label="Format"
                value={formValues.poi_data_format_id}
                placeholder="POI Label"
              />
            </div>
            <div>
              <Input
                id="search_scope"
                type={FIELD_TYPE.TEXT}
                autoComplete="search_scope"
                onChange={onChange}
                label="Search Scope"
                value={formValues.search_scope}
                placeholder=""
              />
            </div>
            <div>
              <Input
                id="is_searchable"
                type={FIELD_TYPE.CHECKBOX}
                autoComplete="is_searchable"
                onChange={onChange}
                label={<div>{icon.FIELD_STATUS.SEARCHABLE} Searchable</div>}
                value={formValues.is_searchable}
              />
            </div>
            <hr />
            <div>
              <Input
                id="is_published_default"
                type={FIELD_TYPE.CHECKBOX}
                autoComplete="is_published_default"
                onChange={onChange}
                label={
                  <div>
                    {formValues.is_published_default
                      ? icon.FIELD_STATUS.PUBLISHED
                      : icon.FIELD_STATUS.UNPUBLISHED}{" "}
                    Default: Published
                  </div>
                }
                value={formValues.is_published_default}
              />
            </div>

            <div>
              <Input
                id="is_locked_default"
                type={FIELD_TYPE.CHECKBOX}
                autoComplete="is_locked_default"
                onChange={onChange}
                label={
                  <div>
                    {formValues.is_locked_default
                      ? icon.FIELD_STATUS.LOCKED
                      : icon.FIELD_STATUS.UNLOCKED}{" "}
                    Default: Locked
                  </div>
                }
                value={formValues.is_locked_default}
              />
            </div>
            <div>
              <Input
                id="is_markdown_default"
                type={FIELD_TYPE.CHECKBOX}
                autoComplete="is_markdown_default"
                onChange={onChange}
                label={
                  <div>
                    {formValues.is_markdown_default
                      ? icon.FIELD_STATUS.MARKDOWN
                      : icon.FIELD_STATUS.UNMARKDOWN}{" "}
                    Default: Render Markdown
                  </div>
                }
                value={formValues.is_markdown_default}
              />
            </div>
            <div className={classes.upperRightCta}>
              <Button
                onClick={() => navigate(`/maptool/map/${mapId}`)}
                label={"Cancel"}
                className={classes.smallButton}
              />
              <Button label={" Update "} className={classes.smallButton} />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Edit;
