import React from "react";
import { useParams } from "react-router-dom";
import { AddEdit } from "src/pages/Common/";
import Add from "./Add";
import Edit from "./Edit";
import { validatedId } from "src/utils.js";

const PoiFieldAddEdit = () => {
  const { id: param_id, poi_id: param_poi_id } = useParams();
  const mapId = validatedId(param_id);
  return (
    <AddEdit
      id={param_poi_id}
      mapId={mapId}
      cancelTo={`/maptool/map/${mapId}`}
      title="MAP :: POI Field"
      Edit={Edit}
      Add={Add}
    />
  );
};
export default PoiFieldAddEdit;
